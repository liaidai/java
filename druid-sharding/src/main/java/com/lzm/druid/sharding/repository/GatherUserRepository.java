package com.lzm.druid.sharding.repository;

import com.lzm.druid.sharding.model.UserEntity;

public interface GatherUserRepository extends BaseRepository<UserEntity, String> {

}
