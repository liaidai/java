/**
 * @author lzm
 * @date 2020年7月14日
 * @version 1.0
 */
package com.lzm.druid.sharding.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

/**
 * 采集用户实体类
 *
 * @author ZJJ
 */
@Data
@Entity
@Table(name = "cfg_gather_user")
@org.hibernate.annotations.Table(appliesTo = "cfg_gather_user", comment = "采集账户")
@javax.persistence.Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "gatherUserCache")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = -1646357140305131371L;

	/**
	 * 用户ID
	 */
	@Id
	@GeneratedValue(generator = "myGenerator")
	@GenericGenerator(name = "myGenerator", strategy = "uuid")
	@Column(name = "vc_id", columnDefinition = "varchar(40) COMMENT '主键'")
	private String id;

	/**
	 * 用户名
	 */
	@Column(name = "vc_username", columnDefinition = "varchar(50) COMMENT '用户名'")
	private String username;

	/**
	 * 密码
	 */
	@Column(name = "vc_password", columnDefinition = "varchar(50) COMMENT '密码'")
	private String password;

	/**
	 * 网站类型：Facebook、Twitter、LinkedIn
	 */
	@Column(name = "vc_web_type", columnDefinition = "varchar(50) COMMENT '网站类型：Facebook、Twitter、LinkedIn'")
	private String webType;

	/**
	 * 对应网站Cookie,采集需要
	 */
	@Column(name = "vc_cookie", columnDefinition = "varchar(2000) COMMENT 'Cookie信息'")
	private String cookie;

	/**
	 * 对应网站Cookie,Twitter Mobile采集需要
	 */
	@Column(name = "vc_mobile_cookie", columnDefinition = "varchar(2000) COMMENT 'Twitter Mobile Cookie信息'")
	private String mobileCookie;

	/**
	 * CSRF令牌,LinkedIn采集需要/async_get_token,Facebook采集需要的fb_dtsg_ag
	 */
	@Column(name = "vc_token", columnDefinition = "varchar(2000) COMMENT 'Token信息'")
	private String token;

	/**
	 * ajaxpipe_token,Facebook采集需要
	 */
	@Column(name = "vc_async_token", columnDefinition = "varchar(2000) COMMENT 'Token信息'")
	private String asyncToken;

	/**
	 * 创建时间
	 */
	@Column(name = "dt_create_time", columnDefinition = "datetime COMMENT '创建时间'")
	private Date createTime;

	/**
	 * 账号更新时间
	 */
	@Column(name = "dt_update_time", columnDefinition = "datetime COMMENT '账号更新时间'")
	private Date updateTime;

	/**
	 * 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vc_user_id", columnDefinition = "varchar(40) COMMENT '用户ID'")
	private UserEntity creator;

	/**
	 * 状态
	 */
	@Column(name = "vc_status", columnDefinition = "varchar(10) COMMENT '状态'")
	private String status;

	/**
	 * 备注
	 */
	@Column(name = "vc_remark", columnDefinition = "varchar(2000) COMMENT '备注'")
	private String remark;

	/**
	 * 公开使用：1=公开、0=私有
	 */
	@Column(name = "vc_public_use", columnDefinition = "varchar(10) default '1' COMMENT '公开使用：1=公开、0=私有'")
	private String publicUse;

	@Transient
	private String creatorId;

	@Transient
	private String creatorName;

	@Version
	@Column(name = "version", columnDefinition = "bigint(20) DEFAULT 0 COMMENT '乐观锁版本字段'")
	private Long version = 0L;

	/**
	 * twitter专用，15分钟内剩余的api请求次数
	 */
	@Column(name = "vc_limit_remaining", columnDefinition = "varchar(10)  COMMENT 'twitter专用，15分钟内剩余的api请求次数'")
	private String limitRemaining;

	/**
	 * twitter专用，api请求次数重置时间
	 */
	@Column(name = "dt_limit_reset_time", columnDefinition = "datetime COMMENT 'twitter专用，api请求次数重置时间'")
	private Date limitResetTime;
}
