package com.log.websocket.aop;

/**
 * @author LZM
 * @date 2020/9/8
 */

import com.log.websocket.vo.GatherTaskVO;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.jboss.logging.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author LZM
 * @date 2020/9/8
 */
@Slf4j
@Order(1)
@Aspect
@Component
public class LogAspect {


    /**
     * 所有的业务类的类名都是xxSpiderxxImpl，统一入口都是gatherData方法
     */
    @Pointcut("execution(* com.log..*.service..*Spider*Impl.gatherData(..))")
    public void pointCut() {}

    @Before("pointCut()")
    public void before(JoinPoint joinPoint){
        //切点已经确定是com.log..*.service..*Spider*Impl.gatherData(..)，该方法的参数只有一个，且为GatherTaskVO
        GatherTaskVO vo = (GatherTaskVO)joinPoint.getArgs()[0];
        //将任务id作为traceId
        MDC.put("traceId", vo.getId());
    }

    @After("pointCut()")
    public void after(JoinPoint joinPoint){
        //方法执行完成以后，删除traceId
        MDC.remove("traceId");
    }
}
