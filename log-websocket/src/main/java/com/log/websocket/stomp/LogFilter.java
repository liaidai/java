package com.log.websocket.stomp;

import java.util.Map;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.filter.LevelFilter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.AbstractMatcherFilter;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * @author B8-02
 */
@Slf4j
public class LogFilter extends AbstractMatcherFilter<ILoggingEvent> {

	Level level;

	@Override
	public FilterReply decide(ILoggingEvent event) {
		if (!isStarted()) {
			return FilterReply.NEUTRAL;
		}
		//过滤指定级别的日志
		if(event.getLevel().equals(level)){
			Map<String, String> mdcMap = event.getMDCPropertyMap();
			String tracId = mdcMap.get("traceId");
			//过滤日志中带有traceId的日志，其他的不需要，traceId使用aop添加
			if(StringUtils.isNotBlank(tracId)){
				return FilterReply.ACCEPT;
			}
		}
		return FilterReply.DENY;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	@Override
	public void start() {
		if (this.level != null) {
			super.start();
		}
	}
}
