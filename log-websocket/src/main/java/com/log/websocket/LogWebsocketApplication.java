package com.log.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lzm
 */
@SpringBootApplication
public class LogWebsocketApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogWebsocketApplication.class, args);
	}

}
