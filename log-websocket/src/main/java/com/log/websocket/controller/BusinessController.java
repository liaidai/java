package com.log.websocket.controller;

import com.log.websocket.service.FacebookSpiderService;
import com.log.websocket.service.FacebookTouchSpiderService;
import com.log.websocket.service.GatherTaskService;
import com.log.websocket.service.TwitterSpiderMobileService;
import com.log.websocket.vo.GatherTaskVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LZM
 * @date 2020/9/9
 */
@RestController
@Slf4j
@RequestMapping("/buss")
public class BusinessController {

    @Resource
    private TwitterSpiderMobileService twitterSpiderMobileService;

    @Resource
    private FacebookSpiderService facebookSpiderService;

    @Resource
    private FacebookTouchSpiderService facebookTouchSpiderService;

    @Resource
    private GatherTaskService gatherTaskService;



    @GetMapping("twitterSpider")
    public String twitterSpider(String taskId){
        GatherTaskVO vo = new GatherTaskVO(taskId);
        twitterSpiderMobileService.gatherData(vo);
        return "spider";
    }


    @GetMapping("facebookSpider")
    public String facebookSpider(String taskId){
        GatherTaskVO vo = new GatherTaskVO(taskId);
        facebookSpiderService.gatherData(vo);
        facebookTouchSpiderService.gatherData(vo);
        return "spider";
    }


    @RequestMapping("task")
    public String task(String taskId){
        gatherTaskService.getTask(taskId);
        return "task";
    }




}
