package com.log.websocket.controller;

import com.log.websocket.websocket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author LZM
 * @date 2020/9/8
 */
@Slf4j
@Controller
public class IndexController {

    @Resource
    private WebSocketServer webSocketServer;


    @GetMapping("/index")
    public String index(){
        return "websocket";
    }

    @GetMapping("/taskIndex")
    public ModelAndView taskIndex(String taskId){
        ModelAndView model = new ModelAndView();
        model.addObject("taskId",taskId);
        model.setViewName("gatherTaskLog");
        return model;
    }


    @GetMapping("/test")
    @ResponseBody
    public String test(){
        ConcurrentHashMap<String,WebSocketServer> map =  webSocketServer.getWebSocketMap();
       map.keySet().stream().forEach(key->{
           WebSocketServer server = map.get(key);
           log.debug("server>>>{}",server);
           try {
               server.sendMessage("123456");
           } catch (IOException e) {
               e.printStackTrace();
           }
       });
       return "123";
    }


}
