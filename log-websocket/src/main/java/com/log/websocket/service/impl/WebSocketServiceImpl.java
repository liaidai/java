package com.log.websocket.service.impl;

import com.log.websocket.service.WebSocketService;
import com.log.websocket.websocket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author LZM
 * @date 2020/9/10
 */
@Slf4j
@Service
public class WebSocketServiceImpl implements WebSocketService {

    @Resource
    private WebSocketServer webSocketServer;

    @Override
    public void sendMessage(String taskId, String logMessage) {
        try {
            ConcurrentHashMap<String, WebSocketServer> map =  webSocketServer.getWebSocketMap();
            WebSocketServer server =  map.get(taskId);
            if(server!=null){
                server.sendMessage(logMessage);
            }else{
                log.warn("客户端已退出");
            }
        } catch (IOException e) {
            log.error("向客户端发送消息时出现异常，异常原因:{}",e.getMessage(),e);
        }
    }
}
