package com.log.websocket.service;

/**
 * 处理websocket相关的业务
 * @author LZM
 * @date 2020/9/10
 */
public interface WebSocketService {

    void sendMessage(String taskId, String logMessage);
}
