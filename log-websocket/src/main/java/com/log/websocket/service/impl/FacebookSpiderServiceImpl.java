package com.log.websocket.service.impl;

import com.log.websocket.service.FacebookSpiderService;
import com.log.websocket.vo.GatherTaskVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author LZM
 * @date 2020/9/9
 */
@Slf4j
@Service
public class FacebookSpiderServiceImpl implements FacebookSpiderService {
    @Override
    public void gatherData(GatherTaskVO taskVO) {
        log.debug("debug>>>FacebookSpiderService实现类方法>>>>>任务id:{}",taskVO.getId());
//        log.info("info>>>>FacebookSpiderService实现类方法>>>>>任务id:{}",taskVO.getId());
    }
}
