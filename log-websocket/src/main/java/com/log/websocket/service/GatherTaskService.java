package com.log.websocket.service;

/**
 * @author LZM
 * @date 2020/9/9
 */
public interface GatherTaskService {

    void getTask(String taskId);

}
