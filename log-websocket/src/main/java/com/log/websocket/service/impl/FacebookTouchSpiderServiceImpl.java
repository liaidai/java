package com.log.websocket.service.impl;

import com.log.websocket.service.FacebookTouchSpiderService;
import com.log.websocket.vo.GatherTaskVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author LZM
 * @date 2020/9/9
 */
@Slf4j
@Service
public class FacebookTouchSpiderServiceImpl implements FacebookTouchSpiderService {
    @Override
    public void gatherData(GatherTaskVO taskVO) {
        log.debug("debug>>>FacebookTouchSpiderService实现方法>>>>对应的任务id：{}",taskVO.getId());
//        log.debug("info>>>FacebookTouchSpiderService实现方法>>>>对应的任务id：{}",taskVO.getId());
    }
}
