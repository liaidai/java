package com.log.websocket.service.impl;

import com.log.websocket.service.GatherTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author LZM
 * @date 2020/9/9
 */
@Slf4j
@Service
public class GatherTaskServiceImpl implements GatherTaskService {
    @Override
    public void getTask(String taskId) {
        log.debug("根据任务id{}获取任务",taskId);
    }
}
