package com.log.websocket.service.impl;

import com.log.websocket.service.TwitterSpiderMobileService;
import com.log.websocket.vo.GatherTaskVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author LZM
 * @date 2020/9/9
 */
@Slf4j
@Service
public class TwitterSpiderMobileServiceImpl implements TwitterSpiderMobileService {
    @Override
    public void gatherData(GatherTaskVO taskVO) {
        log.debug("debug>>>TwitterSpiderMobileService实现类方法<<<<任务id：{}",taskVO.getId());
        log.info("info>>>TwitterSpiderMobileService实现类方法<<<<任务id：{}",taskVO.getId());
    }
}
