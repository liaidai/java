package com.log.websocket.service;

import com.log.websocket.vo.GatherTaskVO;

/**
 * @author LZM
 * @date 2020/9/9
 */
public interface TwitterSpiderMobileService {
    void gatherData(GatherTaskVO taskVO);
}
