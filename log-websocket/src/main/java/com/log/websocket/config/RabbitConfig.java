package com.log.websocket.config;

import com.rabbitmq.client.AMQP;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author LZM
 * @date 2020/9/10
 */
//@Configuration
public class RabbitConfig {

    @Value("${spring.rabbitmq.addresses}")
    private String addresses;

    /**
     * 用户名
     */
    @Value("${spring.rabbitmq.username}")
    private String username;

    /**
     * 密码
     */
    @Value("${spring.rabbitmq.password}")
    private String password;

    @Value("${spring.rabbitmq.virtual-host}")
    private String virtualHost;


    @Value("${platform.parameter.queueName}")
    private String LOG_QUEUE_NAME ;

    @Value("${platform.parameter.exhcangeName}")
    private  String LOG_DIRECT_EXCHANGE_NAME ;

    @Value("${platform.parameter.bindingKey}")
    private  String  LOG_DIRECT_BINDING;


//    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setAddresses(this.addresses);
        connectionFactory.setUsername(this.username);
        connectionFactory.setPassword(this.password);
        connectionFactory.setVirtualHost(this.getVirtualHost(this.username));
        connectionFactory.setChannelCacheSize(50);
        return connectionFactory;
    }


//    @Bean
    public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }


//    @Bean
    public Queue logDirectQueue(){
        return new Queue(LOG_QUEUE_NAME,true);
    }

//    @Bean
    public DirectExchange logDirectExchange(){
        return new DirectExchange(LOG_DIRECT_EXCHANGE_NAME,true,false);
    }

//    @Bean
    public Binding logDirectBinding(Queue logDirectQueue, DirectExchange logDirectExchange){
        return BindingBuilder.bind(logDirectQueue).to(logDirectExchange).with(LOG_DIRECT_BINDING);
    }

    public String getVirtualHost(String username) {
        String vhost = "/";
        if (StringUtils.isNotBlank(this.virtualHost)) {
            return this.virtualHost;
        }
        if (!"guest".equals(username) || !"admin".equals(username)) {
            vhost = username;
        }
        return vhost;
    }



}
