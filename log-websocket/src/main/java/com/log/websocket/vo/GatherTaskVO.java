package com.log.websocket.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LZM
 * @date 2020/9/9
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GatherTaskVO implements Serializable {


    private static final long serialVersionUID = 2366119020173372681L;

    private String id;

    private String taskName;


    public GatherTaskVO(String id) {
        this.id = id;
    }
}
