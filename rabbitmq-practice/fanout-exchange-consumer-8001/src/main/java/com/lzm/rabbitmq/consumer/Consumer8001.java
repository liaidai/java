package com.lzm.rabbitmq.consumer;

import java.io.IOException;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author LZM
 * @date 2020/7/6
 */
@Component
@Slf4j
public class Consumer8001 {

    @RabbitHandler
    //    @RabbitListeners(value = {
//            @RabbitListener(
//                    bindings = @QueueBinding(
//                            value = @Queue(name = "queue8001", durable = "true"),
//                            exchange = @Exchange(name = "my-fanout-exchange-1", type = ExchangeTypes.FANOUT)
//                    ),
//                    concurrency = "1",
//                    priority = "1",
//                    containerFactory = "prefetchCountFactory"
//            ),
//            @RabbitListener(
//                    bindings = @QueueBinding(
//                            value=@Queue(name = "queue8021", durable = "true"),
//                            exchange = @Exchange(name = "my-fanout-exchange-1", type = ExchangeTypes.FANOUT)),
//                    concurrency = "2",
//                    priority = "2",
//                    containerFactory = "prefetchCountFactory"
//            )
//        }
//    )
//    @RabbitListener(queues = "${platform.consumer-queue-name}")
    @RabbitListener(queuesToDeclare = {
            @Queue(value = "${platform.consumer-queue-name[0]}",durable = "true"),
            @Queue(value = "${platform.consumer-queue-name[1]}",durable = "true")
    })
    // @RabbitListener(queues = { "${platform.consumer-queue-name}" }, containerFactory = "prefetchCountFactory")
    public void consumeMessage(String msg, Channel channel, Message message){
        try {
            log.debug("消费者8001======>"+msg);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
