package com.lzm.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FanoutExchangeConsumer8001Application {

	public static void main(String[] args) {
		SpringApplication.run(FanoutExchangeConsumer8001Application.class, args);
	}

}
