package com.lzm.rabbitmq.controller;

import javax.annotation.Resource;

import com.lzm.rabbitmq.service.Producer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import lombok.extern.slf4j.Slf4j;


/**
 * @author LZM
 * @date 2020/7/6
 */
@Slf4j
@RestController
public class ProducerController {


    @Resource
    private Producer producer;


    @GetMapping(value = "index")
    public String publicshMessage(){
        producer.publishMessage();
        return ">>>>>>>>>>>>>>消息已发布>>>>>>>>>";
    }


}
