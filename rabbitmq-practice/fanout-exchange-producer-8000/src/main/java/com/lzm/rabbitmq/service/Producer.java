package com.lzm.rabbitmq.service;

import javax.annotation.Resource;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author LZM
 * @date 2020/7/6
 */
@Service
public class Producer {


    @Value("${platform.exchange-name}")
    private String exchangeName;

    @Resource
    private RabbitTemplate rabbitTemplate;

    public void publishMessage(){
        for(int i = 0; i < 100; i++){
            rabbitTemplate.convertAndSend(exchangeName,"","发布消息========>"+i);
        }
    }


}
