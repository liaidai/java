package com.lzm.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FanoutExchangeProducer8000Application {

	public static void main(String[] args) {
		SpringApplication.run(FanoutExchangeProducer8000Application.class, args);
	}

}
