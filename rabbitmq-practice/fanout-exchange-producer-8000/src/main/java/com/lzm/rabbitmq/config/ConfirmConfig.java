package com.lzm.rabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * @author lzm
 * @date 2020/8/10 22:34
 */
@Component
@Slf4j
public class ConfirmConfig implements RabbitTemplate.ReturnCallback, RabbitTemplate.ConfirmCallback {




    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        log.debug("消息id==>{}",correlationData.getId());
        if(ack){
            log.debug("消息{}发送成功",correlationData.getId());
        }else{
            log.debug("消息{}发送失败",correlationData.getId());
            //TODO 异常处理
        }
    }

    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {

    }
}
