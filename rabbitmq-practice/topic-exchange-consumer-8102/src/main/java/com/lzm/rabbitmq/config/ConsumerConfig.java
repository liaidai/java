package com.lzm.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author LZM
 * @date 2020/7/8
 */
@Configuration
public class ConsumerConfig {

    @Value("${platform.exchange-name}")
    private String exchangeName;

    /**
     * 消费者队列名称（指定队列）
     */
    @Value("${platform.consumer-queue-name}")
    private String queueName;

    /**
     * 主题名称
     */
    @Value("${platform.exchange-routing-key}")
    private String exchangeRoutingKey;


    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange(exchangeName);
    }



    @Bean
    @Primary
    public Queue consumerQueue(){
        return  new Queue(queueName,true);
    }



    @Bean
    public Binding binding(Queue queue, TopicExchange topicExchange){
        return BindingBuilder.bind(queue).to(topicExchange).with(exchangeRoutingKey);
    }


}
