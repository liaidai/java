package com.lzm.rabbitmq.consumer;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author LZM
 * @date 2020/7/8
 */
@Component
@Slf4j
public class MyConsumer {


    @RabbitHandler
//    @RabbitListener(queues = { "${platform.consumer-queue-name}" }, containerFactory = "prefetchCountFactory")
    @RabbitListener(queuesToDeclare = @Queue(name="test-queue", durable = "true", arguments = {@Argument(name="x-max-priority", value = "1", type = "java.lang.Long")}))
    public void consumeMessage(String msg, Channel channel, Message message){
        try {
            log.debug("消费者8102======>"+msg);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
