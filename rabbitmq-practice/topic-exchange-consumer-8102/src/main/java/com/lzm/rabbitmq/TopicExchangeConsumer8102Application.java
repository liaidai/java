package com.lzm.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopicExchangeConsumer8102Application {

	public static void main(String[] args) {
		SpringApplication.run(TopicExchangeConsumer8102Application.class, args);
	}

}
