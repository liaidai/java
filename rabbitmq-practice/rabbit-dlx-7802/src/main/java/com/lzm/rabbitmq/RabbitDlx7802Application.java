package com.lzm.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitDlx7802Application {

    public static void main(String[] args) {
        SpringApplication.run(RabbitDlx7802Application.class, args);
    }

}
