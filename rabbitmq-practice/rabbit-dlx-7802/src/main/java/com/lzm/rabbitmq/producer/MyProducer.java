package com.lzm.rabbitmq.producer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @author lzm
 * @date 2020/8/11 22:40
 */
@Component
@Slf4j
public class MyProducer {


    @Value("${platform.exchange-name}")
    private String exchangeName;

    @Resource
    private RabbitTemplate rabbitTemplate;



    public void send(){
        for (int i = 0; i < 20; i++) {
            CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
//            rabbitTemplate.convertAndSend(exchangeName,"","消息==>"+i,message -> {
//                message.getMessageProperties().setExpiration(3000+"");//发送消息时设置消息的超时时间
//                return message;
//            },correlationData);


            rabbitTemplate.convertAndSend(exchangeName,"","消息==>"+i,correlationData);
        }
    }

}
