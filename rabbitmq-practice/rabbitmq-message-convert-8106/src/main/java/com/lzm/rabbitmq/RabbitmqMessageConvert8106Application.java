package com.lzm.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitmqMessageConvert8106Application {

	public static void main(String[] args) {
		SpringApplication.run(RabbitmqMessageConvert8106Application.class, args);
	}

}
