package com.lzm.rabbitmq.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author LZM
 * @date 2020/11/7
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person implements Serializable {

    private static final long serialVersionUID = -3137521067540352813L;

    private String name;

    private int age;
//
//    @JsonFormat(pattern = "YYYY-mm-dd")
//    private LocalDate birthday;

    private String address;

}
