package com.lzm.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LZM
 * @date 2020/7/6
 */
@Configuration
public class ConsumerConfig {
    /**
     * 交换机名称
     */
    @Value("${platform.exchange-name}")
    private String exchangeName;


    /**
     * 消费者队列名称（指定队列）
     */
    @Value("${platform.consumer-queue-name}")
    private String queueName;

    @Bean
    public Queue consumerQueue(){
        return  new Queue(queueName,true);
    }

    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange(exchangeName);
    }

    @Bean
    public Binding binding(Queue queue, FanoutExchange myexchange) {
        return BindingBuilder.bind(queue).to(myexchange);
    }

}
