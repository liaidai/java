package com.lzm.rabbitmq.rabbitconfirm7999.consumer;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author lzm
 * @date 2020/8/11 23:10
 */
@Component
@Slf4j
public class MyConsumer {
    @RabbitHandler
    @RabbitListener(queues = {"${platform.queue-name}"},concurrency = "1")
    public void msgConsumer(String msg, Channel channel, Message message) throws IOException {
        try {
            int temp = 10/0;
            log.info("消息{}消费成功",msg);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            log.error("接收消息过程中出现异常，执行nack");
            //第三个参数为true表示异常消息重新返回队列，会导致一直在刷新消息，且返回的消息处于队列头部，影响后续消息的处理
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
            log.error("消息{}异常",message.getMessageProperties().getHeaders());
        }
    }


//    @RabbitHandler
//    @RabbitListener(queues = {"${platform.queue-name}"},concurrency = "1")
//    public void msgConsumer(String msg, Channel channel, Message message) throws IOException {
//            log.info("接收到消息>>>{}",msg);
//            int temp = 10/0;
//            log.info("消息{}消费成功",msg);
//    }
}
