package com.lzm.rabbitmq.rabbitconfirm7999;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitConfirm7999Application {

    public static void main(String[] args) {
        SpringApplication.run(RabbitConfirm7999Application.class, args);
    }

}
