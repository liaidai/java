package com.lzm.rabbitmq.rabbitconfirm7999.controller;

import com.lzm.rabbitmq.rabbitconfirm7999.producer.MyProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author lzm
 * @date 2020/8/11 22:48
 */
@RestController
@RequestMapping("/")
@Slf4j
public class IndexController {


    @Resource
    private MyProducer myProducer;


    @GetMapping("index")
    public String index(){
        myProducer.send();
        return "消息已发送";
    }

}
