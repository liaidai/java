package com.lzm.rabbitmq.rabbitconfirm7999.config;

import com.rabbitmq.client.AMQP;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import javax.annotation.Resource;

/**
 * @author lzm
 * @date 2020/8/11 22:33
 */
@Order(1)
@Configuration
@Slf4j
public class RabbitConfig {


    @Value("${platform.exchange-name}")
    private String exchangeName;


    @Value("${platform.queue-name}")
    private String queueName;



    @Bean
    public Queue queue(){
        return new Queue(queueName,true);
    }

    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange(exchangeName,true,false);
    }

    @Bean
    public Binding binding(Queue queue, FanoutExchange fanoutExchange){
        return  BindingBuilder.bind(queue).to(fanoutExchange);
    }


}
