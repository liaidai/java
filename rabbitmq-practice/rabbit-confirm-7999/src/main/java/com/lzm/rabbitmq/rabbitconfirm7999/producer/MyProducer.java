package com.lzm.rabbitmq.rabbitconfirm7999.producer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @author lzm
 * @date 2020/8/11 22:40
 */
@Component
@Slf4j
public class MyProducer {


    @Value("${platform.exchange-name}")
    private String exchangeName;

    @Resource
    private RabbitTemplate rabbitTemplate;



    public void send(){
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause)->{
            if(ack){
                log.info("消息{}处理成功",correlationData.getId());
            }else{
                log.info("消息{}处理失败，失败原因{}",correlationData.getId(),cause);
            }
        });

        rabbitTemplate.setReturnCallback((message, replyCode, replyText, exchange, routingKey)->{
            String correlationDataId = message.getMessageProperties().getCorrelationId();
            log.info("消息发送失败，应答码{}，原因{}，交换机{}，路由键{}",replyCode,replyText,exchange,routingKey);
        });


        for (int i = 0; i < 1; i++) {
            CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
            rabbitTemplate.convertAndSend(exchangeName,"","消息==>"+i,correlationData);
//            rabbitTemplate.convertAndSend("test-confirm","","消息==>"+i,correlationData);
        }
    }

}
