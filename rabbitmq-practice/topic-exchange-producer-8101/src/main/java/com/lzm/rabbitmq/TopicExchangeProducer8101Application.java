package com.lzm.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopicExchangeProducer8101Application {

	public static void main(String[] args) {
		SpringApplication.run(TopicExchangeProducer8101Application.class, args);
	}

}
