package com.lzm.rabbitmq.consumer;

import java.io.IOException;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author LZM
 * @date 2020/7/6
 */
@Component
@Slf4j
public class Consumer8004 {

    @RabbitHandler
    @RabbitListener(queues = { "${platform.consumer-queue-name}" }, containerFactory = "prefetchCountFactory")
    public void consumeMessage(String msg, Channel channel, Message message){
        try {
            log.debug("消费者8004======>"+msg);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
