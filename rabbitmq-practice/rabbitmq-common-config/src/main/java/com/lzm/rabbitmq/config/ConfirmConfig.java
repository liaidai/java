package com.lzm.rabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * @author lzm
 * @date 2020/7/12 11:34
 */
//@Component
@Slf4j
public class ConfirmConfig implements RabbitTemplate.ReturnCallback,RabbitTemplate.ConfirmCallback{

    /**
     * Returned message callback.
     * @param message the returned message.
     * @param replyCode the reply code.
     * @param replyText the reply text.
     * @param exchange the exchange.
     * @param routingKey the routing key.
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        retry(message.getMessageProperties().getCorrelationId());
    }


    /**
     * Confirmation callback.
     * @param correlationData correlation data for the callback.
     * @param ack true for ack, false for nack
     * @param cause An optional cause, for nack, when available, otherwise null.
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        try {
            if (ack) {
                log.debug("消息发送到broker（exchange）成功，消息id--[{}]--", correlationData.getId());
            } else {
                log.error("消息发送到broker（exchange）失败,原因: {}", cause);
                retry(correlationData.getId());
            }
        } catch (Exception e) {
            log.error("rabbitmq confirm消息时出现异常，异常原因：" + e.getMessage(), e);
        } finally {
            // TODO 发送消息时，先将correlationData.getId()以及消息本身缓存到redis中，消息处理完成后从redis中将对应的key值删除掉
        }
    }

    /**
     * 待实现
     *
     * @param msgId
     * @author lzm
     */
    private void retry(String msgId) {

        // 定时轮询redis中的key值，将存在的消息重新发送一次
        // 可能会出现重复数据（任务轮询时正在进入的数据或者未删除的正常数据会重复发送）
        // TODO redis对应key值迁移到另外的key值中，重发机制
    }


}
