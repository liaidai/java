package com.lzm.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DirectExchangeProducer8006Application {

	public static void main(String[] args) {
		SpringApplication.run(DirectExchangeProducer8006Application.class, args);
	}

}
