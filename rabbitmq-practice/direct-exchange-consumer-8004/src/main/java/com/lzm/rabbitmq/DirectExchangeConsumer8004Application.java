package com.lzm.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DirectExchangeConsumer8004Application {

	public static void main(String[] args) {
		SpringApplication.run(DirectExchangeConsumer8004Application.class, args);
	}

}
