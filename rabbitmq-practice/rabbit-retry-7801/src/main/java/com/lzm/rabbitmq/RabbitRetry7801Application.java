package com.lzm.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitRetry7801Application {

    public static void main(String[] args) {
        SpringApplication.run(RabbitRetry7801Application.class, args);
    }

}
