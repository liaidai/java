package com.lzm.rabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.ImmediateRequeueMessageRecoverer;
import org.springframework.amqp.rabbit.retry.MessageRecoverer;
import org.springframework.amqp.rabbit.retry.RepublishMessageRecoverer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lzm
 * @date 2020/8/11 22:33
 */
@Order(1)
@Configuration
@Slf4j
public class RabbitConfig {


    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${platform.exchange-name}")
    private String exchangeName;


    @Value("${platform.queue-name}")
    private String queueName;


    @Value("${platform.dead-letter-exchange-name}")
    private String dlxExchangeName;


    @Value("${platform.dead-letter-queue-name}")
    private String dlxQueueName;


    @Value("${platform.dead-letter-routing-key}")
    private String dlxRoutingKey;


    /**
     * 死信交换机
     * @return
     */
    @Bean
    public DirectExchange dlxExchange(){
        return new DirectExchange(dlxExchangeName);
    }

    /**
     * 死信队列
     * @return
     */
    @Bean
    public Queue dlxQueue(){
        return new Queue(dlxQueueName);
    }

    /**
     * 死信队列绑定死信交换机
     * @param dlxQueue
     * @param dlxExchange
     * @return
     */
    @Bean
    public Binding dlcBinding(Queue dlxQueue, DirectExchange dlxExchange){
        return BindingBuilder.bind(dlxQueue).to(dlxExchange).with(dlxRoutingKey);
    }


    /**
     * 业务队列
     * @return
     */
    @Bean
    public Queue queue(){
        Map<String,Object> params = new HashMap<>();
        params.put("x-dead-letter-exchange",dlxExchangeName);//声明当前队列绑定的死信交换机
        params.put("x-dead-letter-routing-key",dlxRoutingKey);//声明当前队列的死信路由键
        return QueueBuilder.durable(queueName).withArguments(params).build();
//        return new Queue(queueName,true);
    }

    /**
     * 业务交换机
     * @return
     */
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange(exchangeName,true,false);
    }

    /**
     * 业务队列和业务交换机的绑定
     * @param queue
     * @param fanoutExchange
     * @return
     */
    @Bean
    public Binding binding(Queue queue, FanoutExchange fanoutExchange){
        return  BindingBuilder.bind(queue).to(fanoutExchange);
    }

//    @Bean
//    public MessageRecoverer messageRecoverer(){
////        return new RepublishMessageRecoverer(rabbitTemplate,"error-exchange","error-routing-key");
//        return new ImmediateRequeueMessageRecoverer();
//    }
//
//
//    @Bean
//    public DirectExchange errorExchange(){
//        return new DirectExchange("error-exchange",true,false);
//    }
//
//    @Bean
//    public Queue errorQueue(){
//        return new Queue("error-queue", true);
//    }
//
//    @Bean
//    public Binding errorBinding(Queue errorQueue, DirectExchange errorExchange){
//        return BindingBuilder.bind(errorQueue).to(errorExchange).with("error-routing-key");
//    }



}
