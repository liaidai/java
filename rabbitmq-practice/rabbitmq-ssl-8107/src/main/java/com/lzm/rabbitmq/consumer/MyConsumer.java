package com.lzm.rabbitmq.consumer;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.lzm.rabbitmq.service.ProducerService;
import com.rabbitmq.client.Channel;


/**
 * @author LZM
 * @date 2020/11/7
 */
@Component
public class MyConsumer {


    @RabbitHandler
    @RabbitListener(queuesToDeclare = {@Queue(name=ProducerService.STRING_QUEUE, durable = "true")})
    public void listenStrMsg(String msg, Channel channel, Message message){
        System.out.println("字符串类型消息>>>{}"+msg);
    }

//    @RabbitHandler
//    @RabbitListener(queuesToDeclare = {@Queue(name=ProducerService.JSON_QUEUE, durable = "true")},containerFactory = "prefetchTenRabbitListenerContainerFactory")
//    public void listenJsonMsg(String msg, Channel channel, Message message){
//        log.debug("json字符串类型消息>>>>{}",msg);
//    }


//    @RabbitHandler
//    @RabbitListener(queuesToDeclare = {@Queue(name=ProducerService.JAVA_OBJECT_QUEUE, durable = "true")},containerFactory = "prefetchTenRabbitListenerContainerFactory")
//    public void listenJavaObjectMsg(Map msg, Channel channel, Message message){
//        message.getMessageProperties().getHeaders().entrySet().stream().forEach(entry->{
//            log.debug("对象类型headers>>>key[{}]-value[{}]",entry.getKey(),entry.getValue());
//        });
//        log.debug("java对象类型消息>>>>>{}",msg);
//    }
//
//
//    @RabbitHandler
//    @RabbitListener(queuesToDeclare = {@Queue(name=ProducerService.JAVA_COLLECTION_QUEUE, durable = "true")},containerFactory = "prefetchTenRabbitListenerContainerFactory")
//    public void listenCollectionsMsg(List msg, Channel channel, Message message){
//        message.getMessageProperties().getHeaders().entrySet().stream().forEach(entry->{
//            log.debug("集合类型headers>>>key[{}]-value[{}]",entry.getKey(),entry.getValue());
//        });
//        log.debug("java对象集合类型的消息>>>>{}",msg);
//    }

}
