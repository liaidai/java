package com.lzm.rabbitmq.service;

import java.util.List;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lzm.rabbitmq.model.Person;

/**
 * @author LZM
 * @date 2020/11/7
 */
@Service
public class ProducerService {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    /**
     * 普通字符串队列
     */
    public final static String STRING_QUEUE = "str_queue";

    /**
     * json字符串队列
     */
    public final static String JSON_QUEUE = "json_queue";


    /**
     * java对象队列
     */
    public final static String JAVA_OBJECT_QUEUE = "java_object_queue";


    /**
     * java集合队列
     */
    public final static String JAVA_COLLECTION_QUEUE = "java_collection_queue";


    /**
     * 发送字符串类型的消息
     * @param msg
     */
    public void sendStringMsg(String msg){
        rabbitTemplate.convertAndSend(STRING_QUEUE, msg);
    }

    /**
     * 发送json格式的字符串
     * @param jsonStr
     */
    public void sendJsonStrMsg(String jsonStr){
        rabbitTemplate.convertAndSend(JSON_QUEUE, jsonStr);
    }


    /**
     * 发送java对象作为消息
     * @param person
     */
    public void sendJavaObjectMsg(Person person){
        rabbitTemplate.convertAndSend(JAVA_OBJECT_QUEUE, person);
    }


    /**
     * 发送java集合作为消息
     * @param list
     */
    public void sendJavaCollectionMsg(List<Person> list){
        rabbitTemplate.convertAndSend(JAVA_COLLECTION_QUEUE, list);
    }
}
