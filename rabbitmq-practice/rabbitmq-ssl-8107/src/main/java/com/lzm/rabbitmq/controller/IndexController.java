package com.lzm.rabbitmq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.lzm.rabbitmq.model.Person;
import com.lzm.rabbitmq.service.ProducerService;

/**
 * @author LZM
 * @date 2020/11/7
 */
@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private ProducerService producerService;

    @GetMapping("/index")
    public String index(){
        producerService.sendStringMsg("i am string");
//        Person zs = new Person("张三",20,  "北京市海淀区");
//        for(int i=0; i<10;i++){
//            producerService.sendJsonStrMsg(JSON.toJSONString(zs));
//        }
//        Person ls = new Person("李四",30,  "北京市昌平区");
//        producerService.sendJavaObjectMsg(ls);
//        List<Person> list = Stream.of(zs,ls).collect(Collectors.toList());
//        producerService.sendJavaCollectionMsg(list);
        return "123";
    }

}
