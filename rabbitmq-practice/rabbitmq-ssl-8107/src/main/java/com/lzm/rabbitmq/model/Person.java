package com.lzm.rabbitmq.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author LZM
 * @date 2020/11/7
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person implements Serializable {

    private static final long serialVersionUID = -3137521067540352813L;

    private String name;

    private int age;

    private String address;

}
