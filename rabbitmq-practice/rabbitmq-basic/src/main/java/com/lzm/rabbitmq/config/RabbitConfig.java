package com.lzm.rabbitmq.config;

import com.lzm.rabbitmq.constant.Constant;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author lzm
 * @date 2020/7/22 22:45
 */
@Component
public class RabbitConfig {

    /**
     * 定义一个可持久化的队列
     * @return
     */
    @Bean
    public Queue firstQueue(){
        return new Queue(Constant.MY_FIRST_QUEUE_NAME,true,false,false);
    }
}
