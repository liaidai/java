package com.lzm.rabbitmq.service;

import com.lzm.rabbitmq.constant.Constant;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author lzm
 * @date 2020/7/22 23:00
 */
@Service
public class ProducerService {

    /**
     * 注入rabbit模板
     */
    @Resource
    private RabbitTemplate rabbitTemplate;

    public void produceMessage(){

        for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend(Constant.MY_FIRST_QUEUE_NAME, "我是消息====>"+i);
        }
    }
}
