package com.lzm.rabbitmq.controller;

import com.lzm.rabbitmq.service.ProducerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author lzm
 * @date 2020/7/22 23:08
 */
@RestController
@Slf4j
@RequestMapping("/")
public class IndexController {

    @Resource
    private ProducerService producerService;

    @GetMapping(value = "index")
    public String sendMessage(){
        producerService.produceMessage();
        return "消息已发送，请查看消息队列";
    }
}
