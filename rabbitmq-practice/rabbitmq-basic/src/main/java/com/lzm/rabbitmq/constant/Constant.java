package com.lzm.rabbitmq.constant;

/**
 * 常量类
 * @author lzm
 * @date 2020/7/22 23:06
 */
public class Constant {

    /**
     * 定义队列名称
     */
    public final static String MY_FIRST_QUEUE_NAME = "firstQueue";
}
