package com.lzm.rabbitmq.consumer;

import com.lzm.rabbitmq.constant.Constant;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消费者示例代码
 * @author lzm
 * @date 2020/7/22 23:26
 */
@Component
@Slf4j
public class MyConsumer {

    @RabbitHandler
    @RabbitListener(queues = { Constant.MY_FIRST_QUEUE_NAME})
    public void consumeMessage(String msg, Channel channel, Message message){
        log.debug(msg);
    }
}
