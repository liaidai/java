package com.lzm.boot.actuator.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lzm
 * @date 2020/9/16 22:30
 */
@Slf4j
@RestController
@RequestMapping("/index")
public class IndexController {

    @GetMapping("index")
    public String index(){
        log.debug("index>>>>>>>>>>>>>>>>>>1");
        log.debug("index>>>>>>>>>>>>>>>>>>2");
        log.debug("index>>>>>>>>>>>>>>>>>>3");
        log.debug("index>>>>>>>>>>>>>>>>>>4");
        log.debug("index>>>>>>>>>>>>>>>>>>5");
        return "123";
    }
}
