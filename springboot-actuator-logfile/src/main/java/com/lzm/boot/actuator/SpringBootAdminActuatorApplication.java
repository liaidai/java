package com.lzm.boot.actuator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAdminActuatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootAdminActuatorApplication.class, args);
    }

}
