package com.lzm.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author lzm
 * @date 2021/1/5 22:37
 */
@SpringBootApplication
@EnableDiscoveryClient
public class CloudConsulPaymentMain8003 {

    public static void main(String[] args) {
        SpringApplication.run(CloudConsulPaymentMain8003.class,args);
    }

}
