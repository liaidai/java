package com.lzm.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author lzm
 * @date 2021/1/5 22:39
 */
@RestController
@Slf4j
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/payment/consul/")
    public String getPaymentById(){
        return "端口号是["+serverPort+"]的consul服务提供者，"+ UUID.randomUUID().toString();
    }

}
