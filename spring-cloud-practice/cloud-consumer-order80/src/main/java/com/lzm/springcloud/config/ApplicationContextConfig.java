package com.lzm.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author lzm
 * @date 2020/6/16 22:08
 */
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced //配置RestTemplate的负载均衡，不配置会出现找不到服务的情况 nested exception is java.net.UnknownHostException: CLOUD-PAYMENT-SERVICE
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
