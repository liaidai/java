package com.lzm.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lzm
 * @date 2021/1/6 10:24
 */
@SpringBootApplication
public class ConsulConfigMain8005 {
    public static void main(String[] args) {
        SpringApplication.run(ConsulConfigMain8005.class,args);
    }
}
