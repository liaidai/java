package com.lzm.springcloud.controller;

import com.lzm.springcloud.properties.OrderProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lzm
 * @date 2021/1/6 10:16
 */
@RestController
@Slf4j
@RefreshScope
public class OrderController {


    /**
     * 当consul配置中的内容发生改变时，使用@ConfigurationProperties注入的参数能够动态获取到值
     */
    @Resource
    private OrderProperties orderProperties;


    /**
     * 当consul配置中的内容发生改变时，使用@Value注入的参数无法动态获取到新的值，只有在当前Controller添加@RefreshScope注解时才可以动态获取值
     */
    @Value("${order.pay-timeout-seconds}")
    private Integer payTimeoutSeconds;

    @Value("${order.create-frequency-seconds}")
    private Integer createFrequencySeconds;


    @GetMapping("/config/order/properties")
    public OrderProperties getByConfigProperties(){
        return orderProperties;
    }


    @GetMapping("/config/order/values")
    public Map<String,Object> getByValue(){
        Map<String,Object> map = new HashMap<>();
        map.put("pay-timeout-seconds", payTimeoutSeconds);
        map.put("create-frequency-seconds", createFrequencySeconds);
        return map;
    }

}
