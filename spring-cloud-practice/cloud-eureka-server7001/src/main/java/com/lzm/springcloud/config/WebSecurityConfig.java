package com.lzm.springcloud.config;


/**
 * @author lzm
 * @date 2021/1/4 23:56
 */
//@Configuration
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        //默认配置下，Spring Security 要求每个强求需要携带 CSRF Token 才可以访问，而 Eureka-Client 是不会进行携带的，因此需要针对 /eureka/** 路径进行禁用，不然就要改 Eureka-Client 的源码啦
//        http.csrf().ignoringAntMatchers("/eureka/**");
//        super.configure(http);
//    }
//}
