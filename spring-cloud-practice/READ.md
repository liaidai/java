# zipkin
## 引入zipkin
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>
```
## pom文件配置
```
spring:
  zipkin:
    base-url: http://127.0.0.1:9411
  sleuth:
    sampler:
      probability: 1 # 介于0和1之间，1表示全部采集，但是性能也最低
```
## 单机zipkin
使用jar包的方式在windows上启动zipkin，使用elasticsearch做为数据存储
```
java -jar zipkin-server-2.21.6-exec.jar --STORAGE_TYPE=elasticsearch --DES_HOSTS=http://localhost:9200
```
> --DES_HOSTS=http://localhost:9200 本地es的地址， 
> --STORAGE_TYPE=elasticsearch 存储方式为es

## kibana查看链路数据
```
GET  /zipkin-span-*/_search
```
响应结果：
```json
{
"hits" : [
  {
    "_index" : "zipkin-span-2021-01-13",
    "_type" : "_doc",
    "_id" : "dc2f459b0c6e210c-fad27ab164d72ebd2c41eccea711507b",
    "_score" : 1.0,
    "_source" : {
      "traceId" : "dc2f459b0c6e210c",
      "duration" : 9395,
      "remoteEndpoint" : {
        "ipv6" : "::1",
        "port" : 59513
      },
      "localEndpoint" : {
        "serviceName" : "cloud-order-service"
      },
      "timestamp_millis" : 1610548744919,
      "kind" : "SERVER",
      "name" : "get /consumer/payment/get/{id}",
      "id" : "dc2f459b0c6e210c",
      "timestamp" : 1610548744919039,
      "tags" : {
        "http.method" : "GET",
        "http.path" : "//consumer/payment/get/1",
        "mvc.controller.class" : "OrderController",
        "mvc.controller.method" : "getPayment"
      }
    }
  }
]
}
```
## logback日志添加traceId字段
在使用 Zipkin 排查问题的时候，我们可能希望能够跟链路的日志进行关联，那么我们可以将链路编号( Zipkin TraceId )记录到日志中，从而进行关联。
这样，在排查该数据记录时，我们就可以拿着 traceId 字段，去查相应的链路信息和日志信息。
例如：
```xml
<encoder>
    <pattern>%X{traceId}-%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
    <charset>UTF-8</charset> <!-- 设置字符集 -->
</encoder>
```


