package com.lzm.ribbon;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lzm
 * @date 2021/1/6 23:16
 */
@Configuration
public class MyRule {

    @Bean
    public IRule ribbonCustomRule() {
        return new RandomRule();
    }

}
