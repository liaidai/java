package com.lzm.springcloud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "payment")
@AllArgsConstructor
@NoArgsConstructor
public class PaymentEntity implements Serializable {

    private static final long serialVersionUID = -3678875966609464984L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(10) COMMENT '主键'")
    private Integer id;

    @Column(name = "serial", columnDefinition = "varchar(200) COMMENT ''")
    private String serial;

}
