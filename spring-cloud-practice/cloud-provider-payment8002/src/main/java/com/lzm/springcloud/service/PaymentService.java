package com.lzm.springcloud.service;

import com.lzm.springcloud.vo.Payment;

/**
 * @author lzm
 * @date 2020/6/15 22:34
 */
public interface PaymentService {
     Payment create(Payment payment);

     Payment getPaymentById(Long id);
}
