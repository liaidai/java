package com.lzm.springcloud.service.impl;

import com.lzm.springcloud.dao.PaymentRepository;
import com.lzm.springcloud.entity.PaymentEntity;
import com.lzm.springcloud.service.PaymentService;
import com.lzm.springcloud.vo.Payment;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lzm
 * @date 2020/6/15 22:35
 */
@Service
public class PaymentServiceImpl implements PaymentService {


    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public Payment create(Payment payment) {

        PaymentEntity entity = new PaymentEntity();
        BeanUtils.copyProperties(payment,entity);

        paymentRepository.save(entity);

        payment.setId(entity.getId());
        return payment;
    }

    @Override
    public Payment getPaymentById(Long id) {
       PaymentEntity entity = paymentRepository.getPaymentById(id);
        Payment payment = new Payment();
        BeanUtils.copyProperties(entity,payment);
        return payment;
    }
}
