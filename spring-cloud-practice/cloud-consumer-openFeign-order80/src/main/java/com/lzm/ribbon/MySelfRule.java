package com.lzm.ribbon;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author lzm
 * @date 2021/1/7 22:51
 */
@Component
public class MySelfRule {

    @Bean
    public IRule myRule(){
        return new RandomRule();
    }

}
