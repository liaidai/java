package com.lzm.springcloud.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lzm
 * @date 2021/1/7 23:14
 */
@Configuration
public class FeignLogConfig {

    @Bean
    Logger.Level feignLoggerLevel(){
        /**
         * 控制feign接口调用的日志，FULL表示全部的日志
         */
        return Logger.Level.FULL;
    }
}
