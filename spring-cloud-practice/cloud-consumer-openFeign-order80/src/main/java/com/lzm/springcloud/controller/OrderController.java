package com.lzm.springcloud.controller;

import com.lzm.springcloud.service.OpenFeignService;
import com.lzm.springcloud.vo.CommonResult;
import com.lzm.springcloud.vo.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author lzm
 * @date 2021/1/7 22:38
 */
@RestController
@Slf4j
public class OrderController {

    @Value("${server.port}")
    private String serverPort;

    @Resource
    private OpenFeignService openFeignService;

    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult<Payment> getPayment(@PathVariable("id") Long id){
        log.debug("调用openFeign接口");
        return openFeignService.getPayment(id);
    }


    /**
     * openFeign默认的请求时间是1秒，超过1秒直接返回timeout。
     * 如果要处理长时间的请求，需要设置超时时间，因为openFeign自动集成了Ribbon，因此设置Ribbon的超时时间即可
     * @return
     */
    @GetMapping("/consumer/payment/openfeign/timeout")
    public String paymentOpenFeignTimeout(){
        return openFeignService.paymentOpenFeignTimeout();
    }


}
