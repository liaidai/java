package com.lzm.springcloud.service;

import com.lzm.springcloud.vo.CommonResult;
import com.lzm.springcloud.vo.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author lzm
 * @date 2021/1/7 22:37
 */
@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface OpenFeignService {

    @GetMapping("/payment/get/{id}")
    CommonResult<Payment> getPayment(@PathVariable("id") Long id);


    @GetMapping("/payment/openfeign/timeout")
    String paymentOpenFeignTimeout();
}
