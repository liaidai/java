package com.lzm.springcloud.dao;

import com.lzm.springcloud.entity.PaymentEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author lzm
 * @date 2020/6/15 22:30
 */
public interface PaymentRepository extends BaseRepository<PaymentEntity,Integer> {

    @Query(value = "select * from payment where id=:id",nativeQuery = true)
    PaymentEntity getPaymentById(@Param("id") Long id);
}
