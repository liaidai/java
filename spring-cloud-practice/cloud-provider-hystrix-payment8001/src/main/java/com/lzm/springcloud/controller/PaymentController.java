package com.lzm.springcloud.controller;

import com.lzm.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author lzm
 * @date 2021/1/9 23:35
 */
@RestController
@Slf4j
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @GetMapping("/hystrix/payment/ok/{id}")
    public String paymentOK(@PathVariable("id")Integer id){
        return paymentService.payMentOk(id);
    }


    @GetMapping("/hystrix/payment/timeout/{id}")
    public String paymentTimeOut(@PathVariable("id")Integer id){
        return paymentService.payMentTimeOut(id);
    }

}
