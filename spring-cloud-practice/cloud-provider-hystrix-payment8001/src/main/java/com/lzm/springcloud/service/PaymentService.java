package com.lzm.springcloud.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author lzm
 * @date 2021/1/9 23:21
 */
@Service
@Slf4j
public class PaymentService {


    public String payMentOk(Integer id){
        log.debug("payMentOk方法，参数:id={}", id);
        return "请求支付服务端payMentOk方法,id值是"+id+"，响应结果ok！！！";
    }

    @HystrixCommand(fallbackMethod = "paymentTimeoutHandler",commandProperties = {
            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="3000") //3秒内完成是正常的，超过3秒异常，执行回调
    })
    public String payMentTimeOut(Integer id){
        int timeOut = 2000;//配置超时时间，默认是5秒
        try {
            log.debug("payMentTimeOut，参数:id={}, sleep时间={}", id, timeOut);
            Thread.sleep(timeOut);
            int age = 10/0; //程序异常也会触发Fallback
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "请求支付服务端带有超时的payMentTimeOut，响应结果为-->TIMEOUT<--";
    }

    public String paymentTimeoutHandler(Integer id){
        log.debug("执行payMentTimeOut方法的降级方法，id值=>{}", id);
        return "执行payMentTimeOut方法的降级方法，id值=>"+id;
    }

}
