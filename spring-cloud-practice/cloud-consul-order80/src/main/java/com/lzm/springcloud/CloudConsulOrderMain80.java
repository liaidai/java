package com.lzm.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author lzm
 * @date 2021/1/5 22:47
 */
@SpringBootApplication
@EnableDiscoveryClient
public class CloudConsulOrderMain80 {

    public static void main(String[] args) {
        SpringApplication.run(CloudConsulOrderMain80.class, args);
    }


}
