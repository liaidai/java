package com.lzm.test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author LZM
 * @date 2021/1/27
 */
public class AqsTest {


    public static void main(String[] args) {
        Lock lock = new ReentrantLock();

        new Thread(()->{
            lock.lock();try {
                try {
                    TimeUnit.SECONDS.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } finally {lock.unlock();}
        },"AAA").start();
        //暂停1秒，保证A线程先执行，模拟BC排队的情况
        try {TimeUnit.SECONDS.sleep(1);} catch (InterruptedException e) {e.printStackTrace();}

        new Thread(()->{
            lock.lock();try {System.out.println("线程BBB进入");} finally {lock.unlock();}
        },"BBB").start();

        new Thread(()->{
            lock.lock();try {System.out.println("线程BBB进入");} finally {lock.unlock();}
        },"CCC").start();

    }
}
