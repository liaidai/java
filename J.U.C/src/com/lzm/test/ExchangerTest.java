package com.lzm.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;

/**
 * @author LZM
 * @date 2021/1/29
 */
public class ExchangerTest
{
    public static void main(String[] args) throws InterruptedException {
        List<String> data = new ArrayList<>();
        Exchanger<List<String>> exchanger = new Exchanger<List<String>>();
        for (int i = 0; i < 1000; i++) {
            new Thread(new Producer(data,exchanger),"producer-thread").start();
        }
        TimeUnit.MILLISECONDS.sleep(100);
        for (int i = 0; i < 1000; i++) {
            new Thread(new Consumer(data,exchanger),"consumer-thread").start();
        }
    }
}

/**
 * 生产者
 */
class Producer implements Runnable
{

    private List<String> data;
    private Exchanger<List<String>> exchanger;

    public Producer(List<String> data, Exchanger<List<String>> exchanger)
    {
        this.data = data;
        this.exchanger = exchanger;
    }

    @Override
    public void run()
    {
        for (int i = 0; i < 1; i++) {
            System.out.println("生产者第"+i+"次提供");
            for (int j = 0; j < 30000; j++) {
                System.out.println("生产者装入---"+i+"------"+j);
                data.add("buffer:"+i+"------"+j);
            }
            System.out.println("生产者装满了，等待和消费者交换");
            try {
                exchanger.exchange(data);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

/**
 * 消费者
 */
class Consumer implements Runnable
{

    private List<String> data;

    private Exchanger<List<String>> exchanger;

    public Consumer(List<String> data, Exchanger<List<String>> exchanger)
    {
        this.data = data;
        this.exchanger = exchanger;
    }

    @Override
    public void run()
    {
        for (int i = 0; i < 1; i++) {
            try {
                data = exchanger.exchange(data);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("消费者第"+i+"次提取数据");
            for (int j = 0; j < 3000; j++) {
                System.out.println("消费者："+data.get(0));
                data.remove(0);
            }
        }
    }
}
