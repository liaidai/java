package com.lzm.test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * CountDownLatch测试类
 * @author LZM
 * @date 2021/1/28
 */
public class CountDownLatchTest {

    public static void main(String[] args) throws Exception{
//        excuteThreadWithSort();
        excuteMoreThread();
    }

    private static void excuteMoreThread() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(5);
        for (int i = 0; i < 5; i++) {
            new Thread(()->{
                System.out.println("线程"+Thread.currentThread().getName()+"--come in---");
                countDownLatch.countDown();
                System.out.println("线程"+Thread.currentThread().getName()+"--执行结束---");
            },"Thread-"+i).start();
        }
        countDownLatch.await();
        System.out.println("main线程执行结束");
    }


    /**
     * 使用CountDownLatch顺序执行ABC三个线程
     */
    private static void excuteThreadWithSort() throws Exception{
        CountDownLatch ca = new CountDownLatch(1);
        CountDownLatch cb = new CountDownLatch(1);
        CountDownLatch cc = new CountDownLatch(1);

        new Thread(new MyWorker(ca, cb), "AAA").start();

        new Thread(new MyWorker(cb, cc), "BBB").start();

        new Thread(new MyWorker(cc), "CCC").start();

        ca.countDown();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("main线程执行结束");
    }



}

class MyWorker implements Runnable
{
    CountDownLatch countDownLatch1;
    CountDownLatch countDownLatch2;

    MyWorker(CountDownLatch c)
    {
        this.countDownLatch1 = c;
    }

    MyWorker(CountDownLatch c1, CountDownLatch c2)
    {
        this.countDownLatch1 = c1;
        this.countDownLatch2 = c2;
    }

    @Override
    public void run() {
        try {
            countDownLatch1.await();
            if (countDownLatch2!=null) {
                countDownLatch2.countDown();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程"+Thread.currentThread().getName()+"执行完成");
    }
}
