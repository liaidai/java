package com.lzm.test;

/**
 * @author LZM
 * @date 2021/1/29
 */
public class ThreadLocalTest {

//	private static ThreadLocal<AtomicInteger> seq = ThreadLocal.withInitial(() -> new AtomicInteger(0));

	private static ThreadLocal<Integer> seq1 = ThreadLocal.withInitial(() -> 0);
//
//	private static A a = new A();
//	private static ThreadLocal<A> threadLocal = ThreadLocal.withInitial(() -> a);

//	public static Integer atoIncr() {
//		return seq.get().getAndIncrement();
//	}

	public static Integer incr() {
		seq1.set(seq1.get() + 1);
		return seq1.get();
	}
//
//	public static int add() {
//		threadLocal.get().setNumber(threadLocal.get().getNumber() + 5);
//		return threadLocal.get().getNumber();
//	}

	public static void main(String[] args) {
        System.out.println("开始执行main方法");
		new Thread(() -> {
			for (int i = 0; i < 3; i++) {
//				System.out.println("线程" + Thread.currentThread().getName() + " seq>>>" + atoIncr());
				 System.out.println("线程"+Thread.currentThread().getName()+" object>>>"+incr());
			}
		}, "AAA").start();
        System.out.println("main方法执行完成");

//		new Thread(() -> {
//			for (int i = 0; i < 3; i++) {
//				System.out.println("线程" + Thread.currentThread().getName() + " seq>>>" + incr());
//				// System.out.println("线程"+Thread.currentThread().getName()+" object>>>"+add());
//			}
//		}, "BBB" + j).start();

//		new Thread(() -> {
//			for (int i = 0; i < 3; i++) {
//				System.out.println("线程" + Thread.currentThread().getName() + " seq>>>" + incr());
//				// System.out.println("线程"+Thread.currentThread().getName()+" object>>>"+add());
//			}
//		}, "CCC" + j).start();
	}

}

class A {
	private int number = 0;

	public int getNumber() {
		return number;
	}

	public void setNumber(int n) {
		number = n;
	}
}
