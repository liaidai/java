package com.lzm.test;

import java.util.concurrent.*;

/**
 * @author LZM
 * @date 2021/2/3
 */
public class ThreadPoolExectionTest {

    public static void main(String[] args) {
        ThreadFactory factory = (Runnable r)->{
            Thread t = new Thread(r);
            t.setUncaughtExceptionHandler((Thread t1, Throwable thr)->{
                // 在此设置异常监控逻辑
                System.out.println("线程"+t1.getName()+"发生异常，异常原因:"+thr.getMessage());
            });
            return t;
        };

        ExecutorService service = new ThreadPoolExecutor(1,1,0, TimeUnit.MILLISECONDS,new LinkedBlockingDeque<>(10),Executors.defaultThreadFactory(),new ThreadPoolExecutor.AbortPolicy());
        //使用自定义的factory，自定义UncaughtExceptionHandler，当线程内部不捕获异常时，使用UncaughtExceptionHandler可以获取到异常信息
        ExecutorService service1 = new ThreadPoolExecutor(1,1,0, TimeUnit.MILLISECONDS,new LinkedBlockingDeque<>(10),factory,new ThreadPoolExecutor.AbortPolicy());
        Runnable runnable = ()-> System.out.println(10/0);
        service.execute(runnable);
        service1.execute(runnable);
        System.out.println("------------------------------------------------------");
        Future f = service.submit(runnable);
        try {
            Object o = f.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            System.out.println("submit时出现异常，异常原因："+e.getMessage());
        } finally {
        }
        service.shutdown();
        service1.shutdown();
    }

}
