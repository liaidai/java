package com.lzm.test;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @author lzm
 * @date 2021/1/28 23:59
 */
public class CyclicBarrierTest {

    public static void main(String[] args) throws InterruptedException {
        CyclicBarrier c = new CyclicBarrier(7, () -> {
            System.out.println("收集齐了7颗龙珠，现在开始召唤神龙~~~~~~");
        });

        for (int i = 1; i <= 7; i++) {
            new Thread(new DragonBall(c), "Thread-" + i).start();
            TimeUnit.SECONDS.sleep(1);
        }
    }

}

class DragonBall implements Runnable {
    CyclicBarrier cyclicBarrier;

    DragonBall(CyclicBarrier cyclicBarrier) {
        this.cyclicBarrier = cyclicBarrier;
    }


    @Override
    public void run() {
        try {
            System.out.println("线程" + Thread.currentThread().getName() + "收集到一颗龙珠，还差" + (cyclicBarrier.getParties()-1-cyclicBarrier.getNumberWaiting()) + "颗龙珠就可以召唤神龙");
            cyclicBarrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}
