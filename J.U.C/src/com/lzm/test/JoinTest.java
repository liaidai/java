package com.lzm.test;

import java.util.concurrent.TimeUnit;

/**
 * @author LZM
 * @date 2021/2/1
 */
public class JoinTest {

    public static void main(String[] args) throws Exception {
        Thread t1 = new Thread(()->{
            System.out.println("线程AAA开始执行");
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("线程AAA执行结束");
        },"AAA");
        Thread t2 =  new Thread(()->{
            System.out.println("线程BBB开始执行");
            try {
                t1.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("线程BBB执行结束");
        },"BBB");
        t1.start();
        t2.start();
    }

}
