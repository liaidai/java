package com.lzm.test;

import java.util.concurrent.TimeUnit;

/**
 * Synchronize的wait()和notify()方法
 * @author LZM
 * @date 2021/1/26
 */
public class SynchronizedWaitNotifyTest {
    public static void main(String[] args) {

        Object lock = new Object();

        new Thread(()->{
            synchronized (lock){
                System.out.println("线程"+Thread.currentThread().getName()+"\t"+"-----come in");
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("线程"+Thread.currentThread().getName()+"\t"+"-----释放");
            }
        },"AAA").start();
        new Thread(()->{
            try {
                System.out.println("线程"+Thread.currentThread().getName()+"\t"+"-----come in");
                TimeUnit.SECONDS.sleep(3);} catch (InterruptedException e) {e.printStackTrace();}
            synchronized (lock){
                lock.notify();
                System.out.println("线程"+Thread.currentThread().getName()+"\t"+"-----唤醒");
            }
        },"BBB").start();
    }
}
