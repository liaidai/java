package com.lzm.test;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 自旋锁
 * @author LZM
 * @date 2021/2/4
 */
public class SpinLock {

    AtomicReference<Thread> reference = new AtomicReference<>();

    public void lock(){
        Thread t = Thread.currentThread();
        while(!reference.compareAndSet(null,t)){
            //TODO 实现业务逻辑
        }
    }
    public void unlock(){
        Thread t = Thread.currentThread();
        while(!reference.compareAndSet(t,null)){
            //TODO 实现业务逻辑
        }
    }
}
