package com.lzm.test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Lock锁的await()和signal()方法
 * @author LZM
 * @date 2021/1/26
 */
public class ReentrantLockWaitNotifyTest {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();

        new Thread(()->{

            lock.lock();
            try {
                System.out.println(Thread.currentThread().getName()+"\t"+"-------------come in");
                condition.await();
                System.out.println(Thread.currentThread().getName()+"\t"+"------被唤醒");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        },"AAA").start();
        new Thread(()->{
            try {TimeUnit.SECONDS.sleep(3);} catch (InterruptedException e) {e.printStackTrace();}
            lock.lock();
            try {
                condition.signal();
                System.out.println(Thread.currentThread().getName()+"\t"+"--------唤醒");
            } finally {
                lock.unlock();
            }
        },"BBB").start();
    }




    private static void condition1() {
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();

        new Thread(()->{
            lock.lock();
            try {
                System.out.println("进入线程AAA");
                condition.await();
                System.out.println("线程AAA阻塞完成");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        },"AAA").start();

        try {
            TimeUnit.SECONDS.sleep(3);} catch (InterruptedException e) {e.printStackTrace();}

        new Thread(()->{
            lock.lock();
            try {
                System.out.println("进入线程BBB，暂停1秒");
                TimeUnit.SECONDS.sleep(1);
                condition.signal();
                System.out.println("线程BBB释放锁");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        },"BBB").start();
    }
}
