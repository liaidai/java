package com.lzm.test;

import java.util.concurrent.TimeUnit;

/**
 * @author LZM
 * @date 2021/2/5
 */
public class VolatileTest {

    public static volatile int temp = 0;

    public static void main(String[] args) {
        new Thread(()->{
            while (temp!=1){}
            System.out.println("线程"+Thread.currentThread().getName()+"执行完成，此时temp的值:"+temp);
        },"t1").start();

        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(2);
                temp = 1;
                System.out.println("线程"+Thread.currentThread().getName()+"设置temp的值为："+temp);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t2").start();
    }
}
