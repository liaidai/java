package com.lzm.test;

import java.util.concurrent.TimeUnit;

/**
 * 死锁测试
 * @author LZM
 * @date 2021/2/8
 */
public class DeadLockTest {

    public static void main(String[] args) {
        Object lock1 = new Object();
        Object lock2 = new Object();

        new Thread(()->{
            synchronized (lock1){
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock2){

                }
            }
        },"t1").start();
        new Thread(()->{
            synchronized (lock2){
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock1){

                }
            }
        },"t2").start();
    }

}
