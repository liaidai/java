package com.lzm.test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * AtomicInteger的ABA问题
 * @author LZM
 * @date 2021/2/4
 */
public class ABATest {

    public static void main(String[] args) throws InterruptedException {
//        AtomicIntegerAbaTest();

        AtomicStampedReference<Integer> reference = new AtomicStampedReference<Integer>(100,1);

        Thread t1 = new Thread(()->{
            try {
                // 暂停1秒让线程2先获取到版本号
                TimeUnit.SECONDS.sleep(1);
                reference.compareAndSet(100,110,reference.getStamp(),reference.getStamp()+1);
                System.out.println("线程"+Thread.currentThread().getName()+"第一次修改后的值："+reference.getReference()+",版本号："+reference.getStamp());
                reference.compareAndSet(110,100,reference.getStamp(),reference.getStamp()+1);
                System.out.println("线程"+Thread.currentThread().getName()+"第二次修改后的值："+reference.getReference()+",版本号："+reference.getStamp());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t1");
        Thread t2 = new Thread(()->{
            try {
                int stamp = reference.getStamp();
                //暂停2秒，让线程1执行完，修改版本号
                TimeUnit.SECONDS.sleep(2);
                boolean flag = reference.compareAndSet(100,120, stamp, stamp+1);
                System.out.println("线程"+Thread.currentThread().getName()+"修改成功："+flag);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t2");
        t1.start();
        t2.start();
    }

    private static void AtomicIntegerAbaTest() throws InterruptedException {
        AtomicInteger atomicInteger = new AtomicInteger(100);

        Thread t1 = new Thread(()->{
            atomicInteger.compareAndSet(100,110);
            System.out.println("线程"+Thread.currentThread().getName()+"第一次修改后的atomicInteger的值"+atomicInteger.get());
            atomicInteger.compareAndSet(110, 100);
            System.out.println("线程"+Thread.currentThread().getName()+"第二次修改后的atomicInteger的值"+atomicInteger.get());
        },"t1");
        Thread t2 = new Thread(()->{
            try {
                //暂停2秒等待线程t1执行完成
                TimeUnit.SECONDS.sleep(2);
                boolean flag = atomicInteger.compareAndSet(100,120);
                System.out.println("线程"+Thread.currentThread().getName()+"是否修改成功："+flag);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t2");
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("atomicInteger的最终值："+atomicInteger.get());
    }

}
