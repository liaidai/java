package com.lzm.test;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @author LZM
 * @date 2021/1/29
 */
public class SemaphoreTest {

    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(1,true);

        for (int i = 0; i < 2; i++) {
            new Thread(()->{
                try {
                    semaphore.acquire();
                    System.out.println("线程"+Thread.currentThread().getName()+"抢到了一个车位，还在排队等待的车辆："+semaphore.getQueueLength());
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println("线程"+Thread.currentThread().getName()+"释放了一个车位");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                }
            },"Thread-"+i).start();
        }
    }
}
