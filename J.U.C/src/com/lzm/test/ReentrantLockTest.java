package com.lzm.test;

import java.util.concurrent.locks.Lock;

/**
 * 可重入锁测试
 * @author LZM
 * @date 2021/1/26
 */
public class ReentrantLockTest {

    public static void main(String[] args) {
        Lock lock = new java.util.concurrent.locks.ReentrantLock();
       new Thread(()->{
           lock.lock();
           try {
               System.out.println("第一次获取锁");
               lock.lock();
               try {
                   System.out.println("第二次获取锁");
               } finally {
                   lock.unlock();
               }
           } catch (Exception e) {
               e.printStackTrace();
           } finally {
               lock.unlock();
           }
       },"AAA").start();

    }

    private void synchronizedReentrantLock(){
        //锁对象
        Object obj = new Object();
        new Thread(()->{
            synchronized (obj){
                System.out.println("线程"+Thread.currentThread().getName()+"\t"+"第一次获取锁");
                synchronized (obj){
                    System.out.println("线程"+Thread.currentThread().getName()+"\t"+"第二次获取锁");
                }
            }
        },"AAA").start();
    }

}
