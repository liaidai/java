package com.lzm.test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * LockSupport的park()和unpark()方法
 * @author lzm
 * @date 2021/1/25 23:47
 */
public class LockSupportParkUnparkTest {
    public static void main(String[] args) {
        sequenceOfPark();
        doublePark();
    }

    private static void doublePark() {
        new Thread(()->{
            LockSupport.unpark(Thread.currentThread());
            LockSupport.unpark(Thread.currentThread());
            System.out.println("先执行两次unpark");
            //两次unpark，两次park，最终程序不能正常结束，因为unpark只能获取到一个许可，不能累加，执行完一次park消费了许可之后，再次执行park就会阻塞
            LockSupport.park();
            LockSupport.park();
          },"AA").start();
    }

    private static void sequenceOfPark() {
        Thread t1 = new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(3);
                LockSupport.park();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t1");
        t1.start();
        //先执行unpark获取一个许可，3秒后再调用park并未阻塞，说明只要线程获取到了许可，先后顺序无影响
        new Thread(()-> LockSupport.unpark(t1),"t2").start();
        System.out.println("任务执行结束");
    }
}
