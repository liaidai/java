package com.lzm.es;

import com.lzm.es.model.StudentEntity;
import com.lzm.es.service.ElasticSearchReactiveTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * 关于ReactiveElasticSearchTemplate相关的方法测试
 * @author LZM
 * @date 2020/11/27
 */
@Slf4j
@SpringBootTest
public class ReactiveElasticSearchTest {

    @Autowired
    private ElasticSearchReactiveTemplateService elasticSearchReactiveTemplateService;

    @Test
    public void testSave(){
        StudentEntity entity = new StudentEntity()
                .setName("张三丰")
                .setAge(108)
                .setBirthday("1918-11-27")
                .setGender("男")
                .setAddress("湖北省武当山");
        elasticSearchReactiveTemplateService.createIndex(entity);
    }

    @Test
    public void testBatchCreateIndex(){
        StudentEntity entity = new StudentEntity()
                .setName("张三丰")
                .setAge(108)
                .setBirthday("1918-11-27")
                .setGender("男")
                .setAddress("湖北省武当山");

        StudentEntity entity2 = new StudentEntity()
                .setName("张无忌")
                .setAge(25)
                .setBirthday("1818-12-17")
                .setGender("男")
                .setAddress("光明顶");


        StudentEntity entity3 = new StudentEntity()
                .setName("敏敏特木尔")
                .setAge(25)
                .setBirthday("1818-05-16")
                .setGender("女")
                .setAddress("元朝大都");


        StudentEntity entity4 = new StudentEntity()
                .setName("空见神僧")
                .setAge(66)
                .setBirthday("1958-11-27")
                .setGender("男")
                .setAddress("开封少林寺");

        StudentEntity entity5 = new StudentEntity()
                .setName("白眉鹰王殷天正")
                .setAge(55)
                .setBirthday("1848-08-16")
                .setGender("男")
                .setAddress("明教光明顶");


        StudentEntity entity6 = new StudentEntity()
                .setName("光明左使杨逍")
                .setAge(36)
                .setBirthday("1866-02-26")
                .setGender("男")
                .setAddress("光明顶");

        StudentEntity entity7 = new StudentEntity()
                .setName("灭绝师太")
                .setAge(58)
                .setBirthday("1846-03-26")
                .setGender("女")
                .setAddress("峨眉金顶");

        List<StudentEntity> list = new ArrayList<>(4);
        list.add(entity);
        list.add(entity2);
        list.add(entity3);
        list.add(entity4);
        list.add(entity5);
        list.add(entity6);
        list.add(entity7);
        elasticSearchReactiveTemplateService.createBatchIndex(list);
    }



}
