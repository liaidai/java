package com.lzm.es;

import com.lzm.es.model.BookEntity;
import com.lzm.es.repository.BookRepository;
import com.lzm.es.service.ElasticSearchService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@SpringBootTest
class ElasticsearchPracticeApplicationTests {

	@Resource
	private ElasticSearchService elasticSearchService;

	@Test
	public void testInsert() {
		BookEntity entity = new BookEntity();
		entity.setId("1")
				.setTitle("Java编程思想")
				.setAuthor("布鲁斯.艾柯尔")
				.setPrice(158L)
                .setAbbreviation("java圣经")
				.setPubdate("2007-06-10");
		elasticSearchService.insert(entity);
	}


	@Test
	public void testGetAndUpdate(){
		BookEntity instance = new BookEntity().setId("1");
		BookEntity entity = elasticSearchService.getById(instance);
		entity.setId("1")
				.setTitle("Java编程思想")
				.setAuthor("布鲁斯.艾柯尔")
				.setPrice(158L)
				.setPubdate("2007-06-10");
		elasticSearchService.update(entity);
	}

	@Test
	public void testDelete(){
		BookEntity instance = new BookEntity().setId("1");
		elasticSearchService.delete(instance);
	}

	@Test
	public void testMultiInsert(){
		List<BookEntity> list = new ArrayList<>();
		BookEntity entity1 = new BookEntity("2", "深入理解Java虚拟机", "2015-10-15", 125, "周志明，周树人","Java虚拟机");
		BookEntity entity2 = new BookEntity("3", "Java并发编程实战",  "2008-05-16", 104, "张三丰，李连杰，周星驰","并发实战");
		BookEntity entity3 = new BookEntity("4", "Maven实战", "1998-12-08", 58, "李世民，李连杰", "Maven实战");
		BookEntity entity4 = new BookEntity("5", "Spring揭秘",  "2016-08-09", 123, "周星驰，周星星，周润发", "源码揭秘");
		BookEntity entity5 = new BookEntity("6", "elasticsearch权威指南", "1986-05-03", 256, "张翠山，张无忌","es权威指南");

		list.add(entity1);
		list.add(entity2);
		list.add(entity3);
		list.add(entity4);
		list.add(entity5);
		elasticSearchService.multiInsert(list);
	}


	/**
	 * term查询
	 */
	@Test
	public void testTermQuery(){
		List<BookEntity> entities = elasticSearchService.searchBooksByPriceInTerm(158L);
		entities.forEach(System.out::println);
	}

	@Test
	public void testTermsQuery(){
		List<BookEntity> entities = elasticSearchService.searchBooksByPubdateInTerms(Stream.of("2015-10-15","1986-05-03").collect(Collectors.toList()));
		entities.forEach(System.out::println);
	}

	@Resource
	private BookRepository bookRepository;
	@Test
	public void testQueryAnnotation(){
		List<BookEntity>  list = bookRepository.queryAuthorInTerms(Stream.of("布鲁斯.艾柯尔\",\"周志明，周树人").collect(Collectors.joining()));
		log.debug("查询结果[{}]",list.size());
	}




}
