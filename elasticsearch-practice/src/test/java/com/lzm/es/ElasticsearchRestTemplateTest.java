package com.lzm.es;

import com.lzm.es.model.StudentEntity;
import com.lzm.es.service.ElasticSearchTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.composite.ParsedComposite;
import org.elasticsearch.search.aggregations.metrics.ParsedAvg;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.SearchScrollHits;
import org.springframework.data.util.Streamable;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author lzm
 * @date 2020/11/27 22:29
 */
@SpringBootTest
@Slf4j
public class ElasticsearchRestTemplateTest {

    @Autowired
    private ElasticSearchTemplateService elasticSearchTemplateService;


    @Test
    public void testAvgAgg(){
        SearchHits<StudentEntity>searchHits = elasticSearchTemplateService.aggretaAvgAge();
        log.debug("命中结果数:{}",searchHits.getTotalHits());
        searchHits.get().forEach(hit->{
            log.debug("命中文档{}",hit.getContent().toString());
        });
        log.debug("{}",searchHits.getAggregations());
        if(searchHits.hasAggregations()){
            ParsedAvg avg =  Objects.requireNonNull(searchHits.getAggregations()).get("avg_of_age");
            log.debug("统计结果>>>{}",avg.getValue());
        }
    }



    @Test
    public void testGroupByGender(){
        SearchHits<StudentEntity>searchHits = elasticSearchTemplateService.countGroupByGender();
        log.debug("命中结果数:{}",searchHits.getTotalHits());
        searchHits.get().forEach(hit->{
            log.debug("命中文档{}",hit.getContent().toString());
        });
        log.debug("{}",searchHits.getAggregations());
        if(searchHits.hasAggregations()){
            Aggregation aggregation =  Objects.requireNonNull(searchHits.getAggregations()).get("group_of_gender");
            log.debug("统计结果类型>>>{}",aggregation.getType());
            log.debug("统计结果名称>>>{}",aggregation.getName());
            ParsedComposite composite = Objects.requireNonNull(searchHits.getAggregations()).get("group_of_gender");
            composite.getBuckets().forEach(bucket->{
                bucket.getKey().forEach((key, value) -> {
                    log.debug("bucket-key>>{},bucket-value>>{}",key,value);
                });

                bucket.getAggregations().forEach(agg->{
                    log.debug("agg-name>>>{}",agg.getName());
                    log.debug("agg-type>>>{}",agg.getType());
                });

                log.debug("agg-doc-count>>>{}",bucket.getDocCount());
            });
        }
    }

    @Test
    public void testScroll(){
        SearchScrollHits<StudentEntity> scroll=  elasticSearchTemplateService.getByAddressWithScroll("光明顶");
        String scrollId = scroll.getScrollId();
        log.debug("scrollId>>>>{}",scrollId);
        long hitsCount = scroll.getTotalHits();
        log.debug("命中数量>>>{}", hitsCount);
        scroll.stream().forEach(hit-> log.debug("实体文档>>>{}",hit));
        log.debug("===========================华丽的分割线==================================================");
        //继续向下翻页
        scroll = elasticSearchTemplateService.getByAddressWithScrollWithScrollId(scrollId);
        scrollId = scroll.getScrollId();
        log.debug("scrollId>>>>{}",scrollId);
        hitsCount = scroll.getTotalHits();
        log.debug("命中数量>>>{}", hitsCount);
        scroll.stream().forEach(hit-> log.debug("实体文档>>>{}",hit));
    }

    @Test
    public void testPageAndSort(){
        SearchHits<StudentEntity> searchHits =  elasticSearchTemplateService.getByAddressWithPageAndOrderByAge("光明顶",1, 2);
        log.debug("命中结果数:{}",searchHits.getTotalHits());
        searchHits.get().forEach(hit->{
            log.debug(hit.getContent().toString());
        });
    }

    @Test
    public void testRangeAndSort(){
        SearchHits<StudentEntity> searchHits =  elasticSearchTemplateService.getByAgeRangeOrderByBirthday(10,100);
        log.debug("命中结果数:{}",searchHits.getTotalHits());
        searchHits.get().forEach(hit-> log.debug(hit.getContent().toString()));
    }

    @Test
    public void testMultiSearch(){
        List<String> queryWord = Stream.of("张无忌","张三丰").collect(Collectors.toList());
        List<SearchHits<StudentEntity>> list = elasticSearchTemplateService.getByMultiAddress(queryWord);
        list.stream().flatMap(Streamable::stream).forEach(hit->{
            log.debug("查询结果{}",hit);
        });
    }

    @Test
    public void testMultiGet(){
        List<StudentEntity>  list = elasticSearchTemplateService.getByMultiIds(Stream.of("zOmrCHYBcLysKyD9_BAX","zemtCHYBcLysKyD95hBZ").collect(Collectors.toList()));
        list.forEach(entity -> log.debug("命中得文档{}",entity));
    }


    @Test
    public void testCreateIndex(){
        StudentEntity entity = new StudentEntity()
                .setName("混元霹雳手成昆")
                .setAge(50)
                .setGender("男")
                .setAddress("嵩山少林寺");

        StudentEntity savedEntity = elasticSearchTemplateService.createIndex(entity);
        log.debug("保存后的文档{}",savedEntity);

    }




}
