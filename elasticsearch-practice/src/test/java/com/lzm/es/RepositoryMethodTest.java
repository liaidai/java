package com.lzm.es;

import com.lzm.es.model.BookEntity;
import com.lzm.es.repository.BookRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 使用方法名的方式查询数据测试
 * @author lzm
 * @date 2020/11/25 22:06
 */
@Slf4j
@SpringBootTest
public class RepositoryMethodTest {

    @Resource
    private BookRepository repository;

    @Test
    public void testAndOperator(){
        List<BookEntity> list = repository.findByTitleAndAuthor("思想", "布鲁斯");
        log.debug("响应结果:{}",list);
    }

    @Test
    public void testOrOperator(){
        //等价查询语句
        //{"query": {"bool": {"should": [{"match": {"author": "周树人"}},{"term": {"price": 104}}]}}}
        Stream<BookEntity> stream = repository.findByAuthorOrPrice("周树人",104);
        stream.forEach(entity->{
            log.debug(entity.toString());
        });
    }

    @Test
    public void testIsOperator(){
        //等价查询语句
        //{"query": {"bool": {"must": [{"query_string": {"default_field": "price","query": "104"}}]}}}
        Stream<BookEntity> stream = repository.findByPriceIs(104);
        stream.forEach(entity->{
            log.debug(entity.toString());
        });
    }

    @Test
    public void testNotOperator(){
        //等价查询语句
        //{"query": {"bool": {"must_not": [{"query_string": {"default_field": "author","query": "张三丰"}}]}}}
        Stream<BookEntity> stream = repository.findByAuthorNot("张三丰");
        stream.forEach(entity->{
            log.debug(entity.toString());
        });
    }

    @Test
    public void testBetweenOperator(){
        //等价查询语句
        //{"query": {"bool": {"must": [{"range": {"price": {"gte": 100,"lte": 200}}}]}}}
        //gte 大于等于，lte 小于等于
        Stream<BookEntity> stream = repository.findByPriceBetween(100,200);
        stream.forEach(entity->{
            log.debug(entity.toString());
        });
    }

    @Test
    public void testLessAndGreaterOperators(){
        repository.findByPriceLessThan(100).forEach(entity->{
            log.debug("价格低于100的文档{}",entity.toString());
        });

        //等价查询语句
        //{"query": {"bool": {"must": [{"range": {"price": {"lte": 100}}}]}}}
        repository.findByPriceLessThanEqual(100).forEach(entity->{
            log.debug("价格小于等于100的文档{}",entity.toString());
        });

        //创建index时指定的mapping中的日期格式就是yyyy-MM-dd。因此查询时候也该遵循该pattern
        repository.findByPubdateGreaterThan("2008-05-16").forEach(entity->{
            log.debug("发布时间超过指定时间的文档{}",entity.toString());
        });

        //等价查询语句
        //{"query": {"bool": {"must": [{"range": {"pubdate": {"gte": "2008-05-16"}}}]}}}
        repository.findByPubdateGreaterThanEqual("2008-05-16").forEach(entity->{
            log.debug("发布时间等于或者超过指定时间的文档{}",entity.toString());
        });
    }


    @Test
    public void testBeforeAndAfterOperator(){
        //小于等于
        //{"query": {"bool": {"must": [{"range": {"pubdate": {"lte": "2008-05-16"}}}]}}}
        repository.findByPubdateBefore("2008-05-16").forEach(entity->{
            log.debug("发布时间早于指定时间的文档{}",entity);
        });

        //大于等于
        //{"query": {"bool": {"must": [{"range": {"pubdate": {"gte": "2008-05-16"}}}]}}}
        repository.findByPubdateAfter("2008-05-16").forEach(entity->{
            log.debug("发布时间晚于指定时间的文档{}",entity);
        });
    }


    @Test
    public void testLikeOperator(){
        // //{"query": {"bool": {"must_not": [{"query_string": {"default_field": "author","query": "张三丰"}}]}}}
        repository.findByAuthorLike("李连杰").forEach(entity->{
            log.debug("名字在中间位置{}",entity.toString());
        });

        repository.findByAuthorLike("周志明").forEach(entity->{
            log.debug("名字在开头位置{}",entity.toString());
        });

        repository.findByAuthorLike("张无忌").forEach(entity->{
            log.debug("名字在结束位置{}",entity.toString());
        });
    }


    /**
     * 以startsWith相关的方法，查询的字段需要是keyword类型的，否则查询的是全部的
     */
    @Test
    public void testStartsWithOperator(){
//        findByAuthorStartsWith方法，不管单词出现在什么位置，都是可以查询出来的，并不是以什么开头
        repository.findByAbbreviationStartsWith("虚拟").forEach(entity->{
            log.debug("在中间位置{}",entity.toString());
        });

        repository.findByAbbreviationStartsWith("并发").forEach(entity->{
            log.debug("在开头位置{}",entity.toString());
        });

        repository.findByAbbreviationStartsWith("指南").forEach(entity->{
            log.debug("在结束位置{}",entity.toString());
        });

        log.debug("======================华丽的分割线===================================");

        //author字段时text类型，因此进行了分词导致该方法失效，使用@query,指定author的类型是keyword可以正常查询
        repository.findByAuthorStartingWith("李连杰").forEach(entity->{
            log.debug("名字在中间位置{}",entity.toString());
        });

        repository.findByAuthorStartingWith("周志明").forEach(entity->{
            log.debug("名字在开头位置{}",entity.toString());
        });

        repository.findByAuthorStartingWith("张无忌").forEach(entity->{
            log.debug("名字在结束位置{}",entity.toString());
        });

        log.debug("======================华丽的分割线===================================");

        //author字段时text类型，因此进行了分词导致该方法失效，使用@query,指定author的类型是keyword可以正常查询
        repository.findByAbbreviationIsStartingWith("虚拟").forEach(entity->{
            log.debug("在中间位置{}",entity.toString());
        });

        repository.findByAbbreviationIsStartingWith("并发").forEach(entity->{
            log.debug("在开头位置{}",entity.toString());
        });

        repository.findByAbbreviationIsStartingWith("指南").forEach(entity->{
            log.debug("在结束位置{}",entity.toString());
        });
    }

    @Test
    public void testContainingOperator(){
        //Containing | Contains | IsContaining效果是一样的
        //等价查询语句{"query": {"bool": {"must": [{"query_string": {"default_field": "title","query": "*java*"}}]}}}
        //等价查询语句{"query": {"bool": {"must": [{"query_string": {"default_field": "title","query": "*java* AND *虚拟机*"}}]}}}
        //query中支持AND 以及 OR拼接
        //创建倒排索引时会把大写转换成小写，因此直接查询大写字母是查询不到的
        repository.findByTitleContaining("java").forEach(entity -> {
            log.debug("查询英文大写字母{}",entity.toString());
        });
        log.debug("===============华丽的分割线=============================");
        repository.findByTitleContains("java").forEach(entity -> {
            log.debug("查询英文大写字母{}",entity.toString());
        });
        log.debug("===============华丽的分割线=============================");
        repository.findByTitleIsContaining("java").forEach(entity -> {
            log.debug("查询英文大写字母{}",entity.toString());
        });
    }


    @Test
    public void testInAndNotInOperator(){
        repository.findByAuthorIn(Stream.of("周志明","张三丰","张无忌").collect(Collectors.toList())).forEach(entity -> {
            log.debug("包含查询{}",entity.toString());
        });
        log.debug("===============华丽的分割线=============================");
        repository.findByAuthorNotIn(Stream.of("周志明","张三丰","张无忌").collect(Collectors.toList())).forEach(entity -> {
            log.debug("不包含查询{}",entity.toString());
        });
    }

    @Test
    public void testSortOperator(){
        //等价查询语句
        // {"query": {"bool": {"must": [{"query_string": {"default_field": "title","query": "编程"}}]}},"sort":[{"price":{"order": "desc"}}]}
        //注意，对于字符串类型的数据排序，不支持分词的数据排序，也就是说字符串只能是keyword类型，对于数字类型，官方建议是窄类型数据，如int，short等排序
        repository.findByTitleOrderByPriceDesc("编程").forEach(entity->{
            log.debug("按照价格倒序排列{}",entity.toString());
        });
    }


    @Test
    public void testQueryWithPageAndSort(){
        Sort sort = Sort.by(new Sort.Order(Sort.Direction.DESC,"price"));
        Pageable pageable = PageRequest.of(0,1,sort);
        repository.findByAuthorIn("李连杰",pageable).forEach(entity -> {
            log.debug("分页排序查询{}",entity);
        });
    }

}
