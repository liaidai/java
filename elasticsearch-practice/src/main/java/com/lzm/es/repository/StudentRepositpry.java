package com.lzm.es.repository;

import com.lzm.es.model.StudentEntity;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;

/**
 * @author LZM
 * @date 2020/11/27
 */
public interface StudentRepositpry extends ReactiveElasticsearchRepository<StudentEntity,String> {

}
