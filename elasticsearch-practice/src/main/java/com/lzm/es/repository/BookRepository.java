package com.lzm.es.repository;

import com.lzm.es.model.BookEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Highlight;
import org.springframework.data.elasticsearch.annotations.HighlightField;
import org.springframework.data.elasticsearch.annotations.HighlightParameters;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.stream.Stream;

/**
 * @author LZM
 * @date 2020/9/13
 */
public interface BookRepository extends ElasticsearchRepository<BookEntity,String> {

    @Highlight(fields = {
            @HighlightField(name="author"),
            @HighlightField(name = "title")
    },
    parameters = @HighlightParameters(
            preTags = "<red>",
            postTags = "</red>",
            fragmentSize = 500,
            numberOfFragments = 3
    ))
    @Query("{\"term\": {\"price\": \"?0\"}}")
    List<BookEntity> queryPriceInTerm(long price);

    /**
     * 如果要处理这种数组类型参数的，需要手动拼接[]内部的字符串，如要查询["周志明，周树人","布鲁斯.艾柯尔"],
     * 那么查询条件只能是拼接为    布鲁斯.艾柯尔","周志明，周树人   这种形式才能正确查询，可用性比较低
     * @param authors
     * @return
     */
    @Query("{\n" +
            "  \"bool\": {\n" +
            "    \"must\": [\n" +
            "      {\"terms\": {\n" +
            "        \"author.raw\": [\n" +
            "          \"?0\"\n" +
            "        ]\n" +
            "      }}\n" +
            "    ]\n" +
            "  }\n" +
            "}")
    List<BookEntity> queryAuthorInTerms(String authors);

    /**
     * 使用以下方法取代queryPubdateInTerms
     * @param pubdates
     * @return
     */
    List<BookEntity> findByPubdateIn(List<String> pubdates);


    /**
     * 直接使用方法名进行查询
     */
    List<BookEntity> findByTitleAndAuthor(String title,String author);

    Stream<BookEntity> findByAuthorOrPrice(String author, long price);

    Stream<BookEntity> findByPriceIs(long price);

    Stream<BookEntity> findByAuthorNot(String author);

    Stream<BookEntity> findByPriceBetween(long low, long high);

    List<BookEntity> findByPriceLessThan(long price);

    List<BookEntity> findByPriceLessThanEqual(long price);

    List<BookEntity> findByPubdateGreaterThan(String date);

    List<BookEntity> findByPubdateGreaterThanEqual(String date);

    Stream<BookEntity>findByPubdateBefore(String date);

    Stream<BookEntity> findByPubdateAfter(String date);

    Stream<BookEntity> findByAuthorLike(String title);

    Stream<BookEntity> findByAbbreviationStartsWith(String abbreviation);

    @Query("{\n" +
            "  \"bool\": {\n" +
            "    \"must\": [\n" +
            "      {\n" +
            "        \"query_string\": {\n" +
            "          \"default_field\": \"author.raw\",\n" +
            "          \"query\": \"?0*\",\n" +
            "          \"analyze_wildcard\": true\n" +
            "        }\n" +
            "      }\n" +
            "    ]\n" +
            "  }\n" +
            "}")
    Stream<BookEntity> findByAuthorStartingWith(String author);

    Stream<BookEntity> findByAbbreviationIsStartingWith(String abbreviation);

    Stream<BookEntity> findByTitleContaining(String title);

    Stream<BookEntity> findByTitleContains(String title);

    Stream<BookEntity> findByTitleIsContaining(String title);

    Stream<BookEntity> findByAuthorIn(List<String> authors);

    Stream<BookEntity> findByAuthorNotIn(List<String> authors);

    Stream<BookEntity> findByTitleOrderByPriceDesc(String title);

    Page<BookEntity> findByAuthorIn(String author, Pageable pageable);
}
