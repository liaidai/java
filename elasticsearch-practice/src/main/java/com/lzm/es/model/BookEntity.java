package com.lzm.es.model;

import com.lzm.es.analyzer.FieldAnalyzer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * @author LZM
 * @date 2020/9/13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TypeAlias("books")
@Document(indexName = "books",shards = 1, replicas = 0, refreshInterval = "-1",createIndex = true)
public class BookEntity implements Serializable {
    @Id
    private  String id;
    /**
     * 书名
     */
    @Field(analyzer = FieldAnalyzer.IK_MAX_WORD,type = FieldType.Text)
    private String title;

    /**
     * 设置时间格式为自定义，使用pattern参数指定日期格式
     */
    @Field(type = FieldType.Date,format = DateFormat.custom,pattern = "yyyy-MM-dd")
    private String pubdate;


    /**
     * 价格
     */
    @Field(type = FieldType.Long)
    private long price;


    /**
     * 作者
     */
    @Field(type = FieldType.Text,analyzer = FieldAnalyzer.IK_MAX_WORD)
    private String author;


    /**
     * 简述
     */
    @Field(type = FieldType.Keyword)
    private String abbreviation;

}
