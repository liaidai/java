package com.lzm.es.model;

import com.lzm.es.analyzer.FieldAnalyzer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * @author LZM
 * @date 2020/11/27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TypeAlias("stud")
@Document(indexName = "students",shards = 1, replicas = 0, refreshInterval = "-1", createIndex = true)
public class StudentEntity implements Serializable {
    @Id
    private String id;

    @Field(type=FieldType.Text,analyzer = FieldAnalyzer.IK_MAX_WORD)
    private String name;

    @Field(type = FieldType.Integer)
    private int age;

    @Field(type = FieldType.Keyword)
    private String gender;

    @Field(type = FieldType.Date, format = DateFormat.custom,pattern = "yyyy-MM-dd")
    private String birthday;

    @Field(type = FieldType.Text, name = "address", analyzer = FieldAnalyzer.IK_MAX_WORD)
    private String address;
}
