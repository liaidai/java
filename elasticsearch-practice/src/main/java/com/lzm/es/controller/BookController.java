package com.lzm.es.controller;

import com.lzm.es.model.BookEntity;
import com.lzm.es.repository.BookRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.swing.text.html.Option;
import java.util.Optional;

/**
 * @author LZM
 * @date 2020/9/13
 */
@Slf4j
@RestController
@RequestMapping("/book")
public class BookController{

    @Resource
    private BookRepository bookRepository;


    /**
     * 根据id查询
     * @param id
     */
    @GetMapping("/get/{id}")
    public void getById(@PathVariable("id")String id){
        Optional<BookEntity>  optional = bookRepository.findById(id);
        optional.ifPresent(x->{
            log.debug(x.toString());
        });
    }


    /**
     * 新增
     * @param entity
     */
    @PostMapping("/save")
    public void save(BookEntity entity){
        //http://localhost:7788/book/save?title=solr%20action&price=80&pubdate=2004-08-05&author=smith,tom,jerry
        entity = bookRepository.save(entity);
        log.debug(entity.toString());
    }


    /**
     * 修改
     * @param entity
     */
    @PostMapping("/update")
    public void update(BookEntity entity){
        String id = entity.getId();
        entity = bookRepository.save(entity);
        log.debug(entity.toString());
    }

    /**
     * 删除
     * @param id
     */
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id){
        bookRepository.deleteById(id);
//        bookRepository.deleteAll();
    }


}
