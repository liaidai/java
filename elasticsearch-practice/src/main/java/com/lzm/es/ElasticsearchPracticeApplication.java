package com.lzm.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author B8-02
 */
@SpringBootApplication
public class ElasticsearchPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElasticsearchPracticeApplication.class, args);
	}

}
