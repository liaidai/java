package com.lzm.es.service;

import com.lzm.es.model.StudentEntity;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.SearchScrollHits;

import java.util.List;

/**
 * ElasticsearchTemplate相关方法测试
 * @author LZM
 * @date 2020/11/27
 */
public interface ElasticSearchTemplateService {


    /**
     * 对单个文档创建索引
     * @param entity
     */
    StudentEntity createIndex(StudentEntity entity);

    /**
     * 批量创建索引
     * @param entities
     * @return
     */
    List<StudentEntity> batchCreateIndex(List<StudentEntity> entities);

    /**
     * 批量创建索引
     * @param list
     */
    void createBatchIndex(List<StudentEntity> list);


    /**
     * 更新索引
     * @param entity
     */
    void updateIndex(StudentEntity entity);


    /**
     * 根据名字删除索引
     * @param name
     */
    void deleteByName(String name);


    /**
     * multiget
     * @param ids
     */
    List<StudentEntity>  getByMultiIds(List<String> ids);


    /**
     * 根据名字查询文档
     * @param addresses
     * @return
     */
    List<SearchHits<StudentEntity>> getByMultiAddress(List<String> addresses);


    /**
     * 查询年龄段并使用生日排序
     * @param ageFrom
     * @param ageTo
     * @return
     */
    SearchHits<StudentEntity> getByAgeRangeOrderByBirthday(int ageFrom, int ageTo);


    /**
     * 分页查询地址，并且按照年龄排序
     * @param address
     * @param page
     * @param pageSize
     * @return
     */
    SearchHits<StudentEntity>  getByAddressWithPageAndOrderByAge(String address, int page, int pageSize);


    /**
     * 使用scroll查询
     * @param address
     * @return
     */
    SearchScrollHits<StudentEntity> getByAddressWithScroll(String address);


    /**
     * 已知scrollId的情况下继续翻页
     * @param scrollId
     * @return
     */
    SearchScrollHits<StudentEntity> getByAddressWithScrollWithScrollId(String scrollId);


    /**
     * 按照性别统计各自的人数
     */
    SearchHits<StudentEntity> countGroupByGender();


    /**
     * 统计平均年龄
     * @return
     */
    SearchHits<StudentEntity> aggretaAvgAge();
}
