package com.lzm.es.service.impl;

import com.lzm.es.model.StudentEntity;
import com.lzm.es.service.ElasticSearchReactiveTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * ReactiveElasticsearchTemplate测试接口实现类
 * @author LZM
 * @date 2020/11/27
 */
@Slf4j
@Service
public class ElasticSearchReactiveTemplateServiceImpl implements ElasticSearchReactiveTemplateService {

    //TODO webflux对应的方法都不熟悉，暂时先放弃该方式

    @Autowired
    private ReactiveElasticsearchTemplate reactiveElasticsearchTemplate;

    @Override
    public void createIndex(StudentEntity entity) {
//        Mono<StudentEntity> mono = reactiveElasticsearchTemplate.save(entity);
        Mono<StudentEntity> mono = reactiveElasticsearchTemplate.save(entity, IndexCoordinates.of("stud"));
        mono.doOnNext(System.out::println).subscribe();
    }

    @Override
    public void createBatchIndex(List<StudentEntity> list) {
        Flux<StudentEntity> flux = reactiveElasticsearchTemplate.saveAll(list,IndexCoordinates.of("stud"));
        flux.collectList().doOnNext(System.out::println).subscribe();
    }


}
