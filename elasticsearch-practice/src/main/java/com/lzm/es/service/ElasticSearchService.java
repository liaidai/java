package com.lzm.es.service;

import com.lzm.es.model.BookEntity;

import java.util.List;

/**
 * @author LZM
 * @date 2020/11/23
 */
public interface ElasticSearchService {


    /**
     * 新增一条记录
     * @param entity
     */
    void insert(BookEntity entity);


    /**
     * 修改一条记录
     * @param entity
     */
    void update(BookEntity entity);


    /**
     * 删除一条记录
     * @param entity
     */
    void delete(BookEntity entity);


    /**
     * 根据id查询一条记录
     * @param entity
     */
    BookEntity getById(BookEntity entity);

    /**
     * bulk批量插入
     * @param list
     */
    void multiInsert(List<BookEntity> list);


    /**
     * term查询，按照价格查询
     * @param price
     * @return
     */
    List<BookEntity> searchBooksByPriceInTerm(long price);


    /**
     * terms查询，按照发布日期查询
     * @param pubdates
     * @return
     */
    List<BookEntity> searchBooksByPubdateInTerms(List<String> pubdates);


    /**
     * 使用nativequery的方式查询
     * @param author
     * @return
     */
    List<BookEntity> getByNativeQuery(String author);
}
