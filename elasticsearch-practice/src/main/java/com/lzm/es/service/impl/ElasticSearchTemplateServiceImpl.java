package com.lzm.es.service.impl;

import com.lzm.es.model.StudentEntity;
import com.lzm.es.service.ElasticSearchTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.bucket.composite.CompositeAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.composite.TermsValuesSourceBuilder;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.SearchScrollHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author LZM
 * @date 2020/11/27
 */
@Slf4j
@Service
public class ElasticSearchTemplateServiceImpl implements ElasticSearchTemplateService {

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;


    @Override
    public StudentEntity createIndex(StudentEntity entity) {
        return elasticsearchRestTemplate.save(entity);
    }

    @Override
    public List<StudentEntity> batchCreateIndex(List<StudentEntity> entities) {
//        elasticsearchRestTemplate.bulkIndex()
        return null;
    }

    @Override
    public void createBatchIndex(List<StudentEntity> list) {
        Iterable<StudentEntity> savedList = elasticsearchRestTemplate.save(list, IndexCoordinates.of("stud"));
        savedList.forEach(entity -> {
            log.debug("批量保存结果:{}",entity);
        });
    }

    @Override
    public void updateIndex(StudentEntity entity) {
        Map<String,Object> params = new HashMap<>(1);
        params.put("age",111);
        UpdateQuery query = UpdateQuery.builder("1")
                .withParams(params)
                .build();

        elasticsearchRestTemplate.update(query,IndexCoordinates.of("stud"));
    }

    @Override
    public void deleteByName(String name) {
        MatchQueryBuilder builder = QueryBuilders.matchQuery("name","张三丰");
        elasticsearchRestTemplate.delete(new NativeSearchQueryBuilder().withQuery(builder).build(),StudentEntity.class,IndexCoordinates.of("stud"));
        log.debug("删除成功");
    }

    @Override
    public List<StudentEntity> getByMultiIds(List<String> ids) {
        String [] fields = new String[]{"name", "age"};
        //等价查询语句
//        GET /_mget
//        {"docs": [{"_index": "students","_id": "zemtCHYBcLysKyD95hBZ","_source":["name","age","birthday"]},{"_index": "students","_id": "zemtCHYBcLysKyD95hBZ","_source":true }]}
        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withIds(ids)
                //TODO 过滤source无效,最终查询结果还是带着全部的字段
                .withFields(fields)
//                .withSourceFilter(new FetchSourceFilter(new String[]{"name","age"},new String[]{"gender","birthday"}))
                .build();
        return elasticsearchRestTemplate.multiGet(query,StudentEntity.class,IndexCoordinates.of("stud"));
    }

    @Override
    public List<SearchHits<StudentEntity>> getByMultiAddress(List<String> addresses) {
        List<NativeSearchQuery> queries =  addresses.stream().map(addr-> new NativeSearchQueryBuilder().withQuery(QueryBuilders.multiMatchQuery(addr,"name")).build()).collect(Collectors.toList());
        return elasticsearchRestTemplate.multiSearch(queries,StudentEntity.class,IndexCoordinates.of("stud"));
    }

    @Override
    public SearchHits<StudentEntity> getByAgeRangeOrderByBirthday(int ageFrom, int ageTo) {

        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.rangeQuery("age").gte(ageFrom).lte(ageTo))
                .withSort(SortBuilders.fieldSort("birthday").order(SortOrder.DESC))
                .build();

        return elasticsearchRestTemplate.search(query,StudentEntity.class,IndexCoordinates.of("stud"));
    }

    @Override
    public SearchHits<StudentEntity> getByAddressWithPageAndOrderByAge(String address, int page, int pageSize) {

        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.fuzzyQuery("address",address))
                .withPageable(PageRequest.of(page-1,pageSize))
                .withSort(SortBuilders.fieldSort("age").order(SortOrder.DESC))
                .build();

        return elasticsearchRestTemplate.search(query,StudentEntity.class);
    }

    @Override
    public SearchScrollHits<StudentEntity> getByAddressWithScroll(String address) {

        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.fuzzyQuery("address",address))
                .withPageable(PageRequest.of(0,2))
                .build();
        return elasticsearchRestTemplate.searchScrollStart(1000,query,StudentEntity.class,IndexCoordinates.of("stud"));
    }

    @Override
    public SearchScrollHits<StudentEntity> getByAddressWithScrollWithScrollId(String scrollId) {
        return elasticsearchRestTemplate.searchScrollContinue(scrollId, 1000, StudentEntity.class, IndexCoordinates.of("stud"));
    }

    @Override
    public SearchHits<StudentEntity> countGroupByGender() {
        try {

            /**
             * GET /students/_search
             * {"size": 0,"aggs": { "group_of_gender": {"composite": {"sources": [{"gender": {"terms": {"field": "gender"}}}]}}}}
             */
            NativeSearchQuery query = new NativeSearchQueryBuilder()
                    .addAggregation(new CompositeAggregationBuilder("group_of_gender", Stream.of(new TermsValuesSourceBuilder("gender_term").field("gender")).collect(Collectors.toList())))
                    .build();
            return elasticsearchRestTemplate.search(query,StudentEntity.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SearchHits<StudentEntity> aggretaAvgAge() {

        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .addAggregation(new AvgAggregationBuilder("avg_of_age").field("age"))
                .build();
        return elasticsearchRestTemplate.search(query,StudentEntity.class);
    }
}
