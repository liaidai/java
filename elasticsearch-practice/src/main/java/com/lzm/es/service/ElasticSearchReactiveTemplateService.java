package com.lzm.es.service;

import com.lzm.es.model.StudentEntity;

import java.util.List;

/**
 * ReactiveElasticsearchTemplate测试接口
 * @author LZM
 * @date 2020/11/27
 */
public interface ElasticSearchReactiveTemplateService {


    /**
     * 创建索引
     * @param entity
     */
    void createIndex(StudentEntity entity);


    /**
     * 批量创建索引
     * @param list
     */
    void createBatchIndex(List<StudentEntity> list);

}
