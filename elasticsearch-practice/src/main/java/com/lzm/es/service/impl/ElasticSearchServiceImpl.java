package com.lzm.es.service.impl;

import com.lzm.es.model.BookEntity;
import com.lzm.es.repository.BookRepository;
import com.lzm.es.service.ElasticSearchService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author LZM
 * @date 2020/11/23
 */
@Slf4j
@Service
public class ElasticSearchServiceImpl implements ElasticSearchService {

    @Resource
    private BookRepository bookRepository;


    @Override
    public void insert(BookEntity entity) {
        bookRepository.save(entity);
    }

    @Override
    public void update(BookEntity entity) {
        bookRepository.save(entity);
    }

    @Override
    public void delete(BookEntity entity) {
        bookRepository.deleteById(entity.getId());
    }

    @Override
    public BookEntity getById(BookEntity entity) {
        Optional<BookEntity> optionalBookEntity = bookRepository.findById(entity.getId());
        return optionalBookEntity.orElseGet(BookEntity::new);
    }

    @Override
    public void multiInsert(List<BookEntity> list) {
        bookRepository.saveAll(list);
    }

    @Override
    public List<BookEntity> searchBooksByPriceInTerm(long price) {
        return bookRepository.queryPriceInTerm(price);
    }

    @Override
    public List<BookEntity> searchBooksByPubdateInTerms(List<String> pubdates) {
        return bookRepository.findByPubdateIn(pubdates);
    }

    @Deprecated
    @Override
    public List<BookEntity> getByNativeQuery(String author) {
        NativeSearchQuery query = new NativeSearchQueryBuilder().withQuery(QueryBuilders.matchQuery("author","张三丰")).build();
        //过时方法，不再推荐使用
        bookRepository.search(query);
        return null;
    }
}
